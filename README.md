# 计数器物联网上位机远程监控软件

#### 介绍
Java开发的上位机软件，用于管理智能工业计数器硬件（红外线计数器，用于自动感应输送带、装车、点包等场景的智能工业记数器，也可以用于计米器、计时器）。基于Modbus 协议，进行数据采集、远程控制。可以RJ45网线、RS485、RS232有线连接，也可Wifi、2G、4G、Lora、NB等无线连接。数据存储在Mysql数据库，生成报表，可导出数据。

#### 软件架构
1.  以下请求为http请求, 前后端格式传输数据格式为json,请求类型post, 具体看源码
![以下请求 为http请求  前后端格式传输数据格式为json  请求类型post  具体看源码](https://images.gitee.com/uploads/images/2021/0924/182357_166152da_2121755.png "屏幕截图.png")
2.  DeviceController类中的相关接口(参数字段的含义请参考数据库)
![DeviceController类中的相关接口(参数字段的含义请参考数据库)](https://images.gitee.com/uploads/images/2021/0924/182414_28af9a9e_2121755.png "屏幕截图.png")
3. FrameController类中的跳转链接
 ![FrameController类中的跳转链接](https://images.gitee.com/uploads/images/2021/0924/182430_1f695cb2_2121755.png "屏幕截图.png")


#### 安装教程

1.  使用Mysql数据库，请自行配置数据库地址。  配件文件： counter\src\main\resources\application.properties
2.  数据库名称：stcounter,初始化脚本在：/数据库stcounter


#### 使用说明

1.  首页![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184520_bab6dd60_2121755.png "微信图片_20200814143357.png")
2.  选择串口![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184552_94508107_2121755.png "选择串口.png")
3.  添加仪表1![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184607_ea244dca_2121755.png "添加仪表1.png")
4.  添加仪表2![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184618_8714bbc4_2121755.png "添加仪表2.png")
5.  添加仪表2_清零![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184632_1d51878e_2121755.png "添加仪表2-清零.png")
6.  计数器参数设置![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184658_60a1ecd2_2121755.png "计数器设置.png")
7.  计数器上下排值![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184711_702b2a90_2121755.png "计数器上下排值.png")
8.  暂停![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184727_648c0cbf_2121755.png "暂停.png")
9.  柱状监控![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184739_c2500c2c_2121755.png "柱状监控.png")
10. 导出数据![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/184805_bc71a333_2121755.png "导出数据.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 作者/咨询
1.  上海令容网络科技有限公司
2.  https://www.ilingrong.com/lidulia.html#_jcp=1
3.  手机/微信 18502187975


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
