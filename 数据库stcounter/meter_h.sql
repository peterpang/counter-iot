/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云RDS
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6172ob29m6eg9007o.mysql.rds.aliyuncs.com:3306
 Source Schema         : stcounter

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/10/2021 11:54:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for meter_h
-- ----------------------------
DROP TABLE IF EXISTS `meter_h`;
CREATE TABLE `meter_h`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `meter_id` int(11) NULL DEFAULT NULL COMMENT '仪表的id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `upper_row` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上排值',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仪表的名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meter_h
-- ----------------------------
INSERT INTO `meter_h` VALUES (1, 199, '2020-07-13 17:57:45', '2', '111');
INSERT INTO `meter_h` VALUES (2, 199, '2020-07-12 17:58:10', '21', '111');
INSERT INTO `meter_h` VALUES (3, 37, '2020-10-10 09:43:03', '21.0942', 'ST76');
INSERT INTO `meter_h` VALUES (4, 38, '2020-10-10 09:44:53', '21.0942', 'cr76');
INSERT INTO `meter_h` VALUES (5, 39, '2020-10-10 09:45:56', '21.0942', 'cr76');
INSERT INTO `meter_h` VALUES (6, 38, '2020-10-10 10:01:15', '2', 'cr76');
INSERT INTO `meter_h` VALUES (7, 39, '2020-10-10 10:01:21', '1', 'cr76');
INSERT INTO `meter_h` VALUES (8, 40, '2020-10-10 10:01:24', '0', 'cr76');
INSERT INTO `meter_h` VALUES (9, 40, '2020-10-10 11:00:50', '0', 'cr76');
INSERT INTO `meter_h` VALUES (10, 41, '2020-10-10 11:45:31', '18', 'st76');
INSERT INTO `meter_h` VALUES (11, 42, '2020-10-10 11:47:01', '85', 'cr76');
INSERT INTO `meter_h` VALUES (12, 41, '2020-10-10 12:00:54', '18', 'st76');
INSERT INTO `meter_h` VALUES (13, 42, '2020-10-10 12:01:56', '85', 'cr76');
INSERT INTO `meter_h` VALUES (14, 41, '2020-10-10 13:00:54', '18', 'st76');
INSERT INTO `meter_h` VALUES (15, 42, '2020-10-10 13:01:55', '85', 'cr76');
INSERT INTO `meter_h` VALUES (16, 41, '2020-10-10 14:00:54', '18', 'st76');
INSERT INTO `meter_h` VALUES (17, 42, '2020-10-10 14:01:55', '85', 'cr76');
INSERT INTO `meter_h` VALUES (18, 41, '2020-10-10 15:00:57', '18', 'st76');
INSERT INTO `meter_h` VALUES (19, 42, '2020-10-10 15:01:58', '85', 'cr76');
INSERT INTO `meter_h` VALUES (20, 41, '2020-10-10 16:00:57', '18', 'st76');
INSERT INTO `meter_h` VALUES (21, 42, '2020-10-10 16:01:58', '85', 'cr76');
INSERT INTO `meter_h` VALUES (22, 42, '2020-10-20 08:50:29', '0', 'cr76');
INSERT INTO `meter_h` VALUES (23, 42, '2020-10-20 09:00:27', '21.0942', 'cr76');
INSERT INTO `meter_h` VALUES (24, 43, '2020-10-20 09:37:18', '10000', '测试com9');
INSERT INTO `meter_h` VALUES (25, 44, '2020-10-20 09:37:23', '10000', '测试com9');
INSERT INTO `meter_h` VALUES (26, 45, '2020-10-20 09:42:10', '10000', '测试com9');
INSERT INTO `meter_h` VALUES (27, 42, '2020-10-22 11:54:01', '85', 'cr76');
INSERT INTO `meter_h` VALUES (28, 46, '2020-10-22 11:59:10', '10', 'st76');
INSERT INTO `meter_h` VALUES (29, 42, '2020-10-22 12:00:01', '85', 'cr76');
INSERT INTO `meter_h` VALUES (30, 46, '2020-10-22 12:00:02', '0', 'st76');
INSERT INTO `meter_h` VALUES (31, 47, '2020-10-22 12:02:48', '0', 'ST76');
INSERT INTO `meter_h` VALUES (32, 48, '2020-10-22 12:03:54', '85', 'CR76');
INSERT INTO `meter_h` VALUES (33, 49, '2020-10-22 12:08:08', '200', 'st');
INSERT INTO `meter_h` VALUES (34, 50, '2020-10-22 12:09:07', '0', 'st');
INSERT INTO `meter_h` VALUES (35, 51, '2020-10-22 12:09:09', '0', 'st');
INSERT INTO `meter_h` VALUES (36, 52, '2020-10-22 12:09:19', '85', 'CR');
INSERT INTO `meter_h` VALUES (37, 53, '2020-10-22 12:09:35', '85', 'CR');
INSERT INTO `meter_h` VALUES (38, 54, '2020-10-22 12:26:02', '0', 'ST76');
INSERT INTO `meter_h` VALUES (39, 54, '2020-10-22 13:58:18', '0', 'ST76');
INSERT INTO `meter_h` VALUES (40, 55, '2020-10-22 13:59:21', '0', 'ST76');
INSERT INTO `meter_h` VALUES (41, 56, '2020-10-22 14:00:38', '0', 'ST76');
INSERT INTO `meter_h` VALUES (42, 57, '2020-10-22 14:02:56', '0', 'CR76');
INSERT INTO `meter_h` VALUES (43, 56, '2020-10-22 15:00:00', '0', 'ST76');
INSERT INTO `meter_h` VALUES (44, 57, '2020-10-22 15:00:21', '85', 'CR76');
INSERT INTO `meter_h` VALUES (45, 58, '2020-10-22 15:22:40', '0', '测试com1');
INSERT INTO `meter_h` VALUES (46, 58, '2020-10-22 16:00:01', '0', '测试com1');
INSERT INTO `meter_h` VALUES (47, 58, '2020-10-22 17:00:02', '0', '测试com1');
INSERT INTO `meter_h` VALUES (48, 58, '2020-10-22 18:00:01', '0', '测试com1');
INSERT INTO `meter_h` VALUES (49, 62, '2020-10-30 10:04:29', '0', 'ST76');
INSERT INTO `meter_h` VALUES (50, 63, '2020-10-30 10:05:43', '36', 'CR76');
INSERT INTO `meter_h` VALUES (51, 62, '2020-10-30 11:00:01', '0', 'ST76');
INSERT INTO `meter_h` VALUES (52, 63, '2020-10-30 11:00:02', '0', 'CR76');
INSERT INTO `meter_h` VALUES (53, 63, '2020-10-30 12:00:00', '0', 'CR76');
INSERT INTO `meter_h` VALUES (54, 62, '2020-10-30 12:00:02', '0', 'ST76');
INSERT INTO `meter_h` VALUES (55, 63, '2020-10-30 13:00:00', '0', 'CR76');
INSERT INTO `meter_h` VALUES (56, 62, '2020-10-30 13:00:01', '0', 'ST76');
INSERT INTO `meter_h` VALUES (57, 63, '2020-10-30 14:00:00', '0', 'CR76');
INSERT INTO `meter_h` VALUES (58, 62, '2020-10-30 14:00:01', '0', 'ST76');
INSERT INTO `meter_h` VALUES (59, 62, '2020-10-30 15:00:00', '0', 'ST76');
INSERT INTO `meter_h` VALUES (60, 63, '2020-10-30 15:00:01', '0', 'CR76');
INSERT INTO `meter_h` VALUES (61, 62, '2020-10-30 16:00:00', '0', 'ST76');
INSERT INTO `meter_h` VALUES (62, 63, '2020-10-30 16:00:01', '0', 'CR76');
INSERT INTO `meter_h` VALUES (63, 62, '2020-10-30 17:00:07', '0', 'ST76');
INSERT INTO `meter_h` VALUES (64, 63, '2020-10-30 17:00:08', '0', 'CR76');
INSERT INTO `meter_h` VALUES (65, 63, '2020-11-02 14:23:57', '0', 'CR76');
INSERT INTO `meter_h` VALUES (66, 62, '2020-11-02 14:23:58', '0', 'ST76');
INSERT INTO `meter_h` VALUES (67, 62, '2020-11-02 15:00:01', '0', 'ST76');
INSERT INTO `meter_h` VALUES (68, 63, '2020-11-02 15:00:02', '0', 'CR76');
INSERT INTO `meter_h` VALUES (69, 64, '2020-11-10 10:40:04', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (70, 64, '2020-11-10 11:00:00', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (71, 64, '2020-11-10 12:00:01', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (72, 64, '2020-11-10 13:00:01', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (73, 64, '2020-11-10 14:00:01', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (74, 64, '2020-11-10 15:00:00', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (75, 64, '2020-11-10 16:57:38', '200', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (76, 64, '2020-11-10 16:57:38', '200', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (77, 64, '2020-11-10 17:00:00', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (78, 64, '2020-11-10 18:00:01', '20003', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (79, 64, '2020-11-17 10:19:49', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (80, 64, '2020-11-17 12:09:19', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (81, 64, '2020-11-17 13:00:17', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (82, 64, '2020-11-17 14:00:24', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (83, 64, '2020-11-17 15:04:16', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (84, 64, '2020-11-17 16:16:00', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (85, 64, '2020-11-17 17:03:17', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (86, 64, '2020-11-17 18:00:56', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (87, 64, '2020-11-17 19:00:09', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (88, 64, '2020-11-23 15:15:46', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (89, 64, '2020-11-23 16:00:45', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (90, 64, '2020-11-23 17:00:50', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (91, 64, '2020-11-24 12:09:13', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (92, 64, '2020-11-25 10:40:11', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (93, 64, '2020-12-24 16:36:15', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (94, 65, '2020-12-29 14:49:25', '0', '00');
INSERT INTO `meter_h` VALUES (95, 66, '2020-12-29 14:54:30', '0', '02');
INSERT INTO `meter_h` VALUES (96, 66, '2020-12-29 15:00:07', '0', '02');
INSERT INTO `meter_h` VALUES (97, 65, '2020-12-29 15:00:08', '0', '00');
INSERT INTO `meter_h` VALUES (98, 67, '2020-12-29 15:08:33', '0.0', '02');
INSERT INTO `meter_h` VALUES (99, 65, '2020-12-29 16:00:18', '0', '00');
INSERT INTO `meter_h` VALUES (100, 67, '2020-12-29 16:00:28', '0.0', '02');
INSERT INTO `meter_h` VALUES (101, 64, '2020-12-29 19:00:19', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (102, 65, '2020-12-29 19:03:14', '0', '00');
INSERT INTO `meter_h` VALUES (103, 67, '2020-12-29 19:07:32', '0', '02');
INSERT INTO `meter_h` VALUES (104, 64, '2021-01-06 17:14:23', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (105, 65, '2021-01-06 17:14:23', '0', '00');
INSERT INTO `meter_h` VALUES (106, 65, '2021-03-30 18:59:46', '0', '00');
INSERT INTO `meter_h` VALUES (107, 64, '2021-03-30 18:59:46', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (108, 67, '2021-03-30 19:00:46', '0', '02');
INSERT INTO `meter_h` VALUES (109, 64, '2021-03-30 20:31:56', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (110, 65, '2021-03-30 20:31:56', '0', '00');
INSERT INTO `meter_h` VALUES (111, 65, '2021-06-09 09:32:54', '0', '00');
INSERT INTO `meter_h` VALUES (112, 64, '2021-06-09 09:32:54', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (113, 65, '2021-06-09 16:02:01', '0', '00');
INSERT INTO `meter_h` VALUES (114, 64, '2021-06-09 16:02:01', '0', '测试Com8ST76');
INSERT INTO `meter_h` VALUES (115, 67, '2021-06-09 16:03:03', '0', '02');

SET FOREIGN_KEY_CHECKS = 1;
