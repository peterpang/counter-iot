/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云RDS
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6172ob29m6eg9007o.mysql.rds.aliyuncs.com:3306
 Source Schema         : stcounter

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/10/2021 11:54:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for modbus_type
-- ----------------------------
DROP TABLE IF EXISTS `modbus_type`;
CREATE TABLE `modbus_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '设备通讯接口id',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'dtu的ip/串口中每个设备的addres',
  `port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'dtu的端口/串口的串口号',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通讯接口的类型（1-tcp，2-gprs，3-串口）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `baudRate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '9600' COMMENT '波特率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 235 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of modbus_type
-- ----------------------------
INSERT INTO `modbus_type` VALUES (234, '1', 'COM4', '3', '2020-08-22 11:13:26', '2020-08-22 11:13:26', '9600');
INSERT INTO `modbus_type` VALUES (235, '1', 'COM3', '3', '2020-08-29 09:43:18', '2020-08-29 09:43:18', '9600');

SET FOREIGN_KEY_CHECKS = 1;
