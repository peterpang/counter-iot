/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云RDS
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6172ob29m6eg9007o.mysql.rds.aliyuncs.com:3306
 Source Schema         : stcounter

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/10/2021 11:54:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for meter_info
-- ----------------------------
DROP TABLE IF EXISTS `meter_info`;
CREATE TABLE `meter_info`  (
  `meter_id` int(11) NOT NULL AUTO_INCREMENT,
  `meter_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `connect_type` int(11) NOT NULL COMMENT '连接方式',
  `com_port` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '串口',
  `baud_rate` int(11) NOT NULL COMMENT '波特率',
  `data_mode` int(11) NOT NULL COMMENT '数据模式：BCD/整型/浮点型',
  `modbus_device_id` int(11) NOT NULL COMMENT '设备id',
  `modbus_pos` int(11) NOT NULL COMMENT '存储地址',
  `alarm1` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警值1',
  `alarm2` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警值2',
  `upper_row_val` int(11) NOT NULL DEFAULT 0 COMMENT '上排值',
  `lower_row_val` int(11) NOT NULL DEFAULT 0 COMMENT '下排值',
  `decimal_digit` int(11) NOT NULL DEFAULT 0 COMMENT '小数点位数',
  `scale_rate` int(11) NOT NULL COMMENT '步进倍率',
  `suspend_flag` int(11) NOT NULL DEFAULT 0 COMMENT '暂停标识（1-暂停）',
  `clear_mode` int(11) NOT NULL DEFAULT 0 COMMENT '清零模式',
  `clear_time` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '清零时间',
  `meter_model` int(11) NOT NULL COMMENT '设备型号',
  `online` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否在线：0，不在线',
  `clear_loop_type` int(11) NOT NULL DEFAULT 0 COMMENT '清零循环方式',
  `last_clear_time` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '上次清零时间',
  PRIMARY KEY (`meter_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meter_info
-- ----------------------------
INSERT INTO `meter_info` VALUES (62, 'ST76', 3, 'COM4', 9600, 1, 1, 1, '100007', '100010', 0, 0, 0, 100000, 0, 2, '2020-11-02 15:04:08', 1, 0, 0, '2020-11-02 14:43:53');
INSERT INTO `meter_info` VALUES (63, 'CR76', 3, 'COM4', 9600, 2, 2, 1, '100005', '100011', 0, 0, 0, 10000, 0, 2, '2020-11-02 15:04:29', 2, 0, 0, '2020-11-02 14:44:36');
INSERT INTO `meter_info` VALUES (64, '测试Com8ST76', 3, 'COM8', 9600, 1, 1, 1, '0', '0', 0, 0, 0, 0, 0, 0, '2020-11-10 10:40:03', 1, 0, 0, '1970-01-01 00:00:00');
INSERT INTO `meter_info` VALUES (65, '00', 3, 'COM5', 9600, 1, 1, 1, '0', '0', 0, 0, 0, 0, 0, 0, '2020-12-29 15:28:13', 1, 0, 0, '1970-01-01 00:00:00');
INSERT INTO `meter_info` VALUES (67, '02', 3, 'COM5', 9600, 1, 2, 1, '0', '0', 0, 0, 0, 0, 0, 0, '2020-12-29 15:08:01', 1, 0, 0, '1970-01-01 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
