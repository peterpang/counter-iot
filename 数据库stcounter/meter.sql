/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云RDS
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6172ob29m6eg9007o.mysql.rds.aliyuncs.com:3306
 Source Schema         : stcounter

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/10/2021 11:53:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for meter
-- ----------------------------
DROP TABLE IF EXISTS `meter`;
CREATE TABLE `meter`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '仪表id\r\n',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仪表的名称',
  `modbus_id` bigint(255) NULL DEFAULT NULL COMMENT '通讯接口id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `upper_row` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '上排值',
  `lower_row` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '下排值',
  `al1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0.00' COMMENT '报警值',
  `al2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报警值',
  `suspend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '暂停默认为开启false',
  `clear_mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '2' COMMENT '1为开启2为关闭',
  `whetherBCD` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否为bcd码',
  `scal` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '计数倍数',
  `register_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '寄存器地址',
  `time` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '时间',
  `upper_row_int` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '上排值是int   默认是1   0为浮点型',
  `off_line` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否离线   默认1 离线   0为在线',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 236 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meter
-- ----------------------------
INSERT INTO `meter` VALUES (234, '整型', 234, '2020-08-22 11:13:26', '2020-08-29 13:08:05', '250', '300', '200', '300', '0', '2', '2', '5', '1', '3', '1', '0');
INSERT INTO `meter` VALUES (235, 'q1', 235, '2020-08-29 09:43:18', '2020-08-29 10:47:18', '0', '0', '0', '0', '0', '2', '2', '0', '1', '3', '1', '1');

SET FOREIGN_KEY_CHECKS = 1;
