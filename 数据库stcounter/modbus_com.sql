/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云RDS
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6172ob29m6eg9007o.mysql.rds.aliyuncs.com:3306
 Source Schema         : stcounter

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/10/2021 11:54:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for modbus_com
-- ----------------------------
DROP TABLE IF EXISTS `modbus_com`;
CREATE TABLE `modbus_com`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '串口的名称',
  `id` int(11) NULL DEFAULT NULL COMMENT 'id'
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of modbus_com
-- ----------------------------
INSERT INTO `modbus_com` VALUES ('COM2', 2);
INSERT INTO `modbus_com` VALUES ('COM1', 1);
INSERT INTO `modbus_com` VALUES ('COM3', 3);
INSERT INTO `modbus_com` VALUES ('COM4', 4);
INSERT INTO `modbus_com` VALUES ('COM5', 5);
INSERT INTO `modbus_com` VALUES ('COM6', 6);
INSERT INTO `modbus_com` VALUES ('COM7', 7);
INSERT INTO `modbus_com` VALUES ('COM8', 8);
INSERT INTO `modbus_com` VALUES ('COM9', 9);
INSERT INTO `modbus_com` VALUES ('COM10', 10);
INSERT INTO `modbus_com` VALUES ('COM11', 11);
INSERT INTO `modbus_com` VALUES ('COM12', 12);
INSERT INTO `modbus_com` VALUES ('COM13', 13);
INSERT INTO `modbus_com` VALUES ('COM14', 14);
INSERT INTO `modbus_com` VALUES ('COM15', 15);
INSERT INTO `modbus_com` VALUES ('COM16', 16);
INSERT INTO `modbus_com` VALUES ('COM17', 17);
INSERT INTO `modbus_com` VALUES ('COM18', 18);
INSERT INTO `modbus_com` VALUES ('COM19', 19);
INSERT INTO `modbus_com` VALUES ('COM20', 20);

SET FOREIGN_KEY_CHECKS = 1;
