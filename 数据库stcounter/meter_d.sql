/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云RDS
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6172ob29m6eg9007o.mysql.rds.aliyuncs.com:3306
 Source Schema         : stcounter

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/10/2021 11:53:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for meter_d
-- ----------------------------
DROP TABLE IF EXISTS `meter_d`;
CREATE TABLE `meter_d`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `meter_id` int(11) NULL DEFAULT NULL COMMENT '仪表的id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `upper_row` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上排值',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仪表的名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meter_d
-- ----------------------------
INSERT INTO `meter_d` VALUES (1, 199, '2020-07-13 18:44:08', '1', '111');
INSERT INTO `meter_d` VALUES (2, 199, '2020-07-01 18:44:38', '2', '111');
INSERT INTO `meter_d` VALUES (3, 37, '2020-10-10 09:43:03', '21.0942', 'ST76');
INSERT INTO `meter_d` VALUES (4, 38, '2020-10-10 09:44:53', '2', 'cr76');
INSERT INTO `meter_d` VALUES (5, 39, '2020-10-10 09:45:56', '1', 'cr76');
INSERT INTO `meter_d` VALUES (6, 40, '2020-10-10 10:01:24', '0', 'cr76');
INSERT INTO `meter_d` VALUES (7, 41, '2020-10-10 11:45:31', '18', 'st76');
INSERT INTO `meter_d` VALUES (8, 42, '2020-10-10 11:47:01', '85', 'cr76');
INSERT INTO `meter_d` VALUES (9, 42, '2020-10-20 08:50:29', '21.0942', 'cr76');
INSERT INTO `meter_d` VALUES (10, 43, '2020-10-20 09:37:18', '10000', '测试com9');
INSERT INTO `meter_d` VALUES (11, 44, '2020-10-20 09:37:23', '10000', '测试com9');
INSERT INTO `meter_d` VALUES (12, 45, '2020-10-20 09:42:10', '10000', '测试com9');
INSERT INTO `meter_d` VALUES (13, 42, '2020-10-22 11:54:01', '85', 'cr76');
INSERT INTO `meter_d` VALUES (14, 46, '2020-10-22 11:59:10', '0', 'st76');
INSERT INTO `meter_d` VALUES (15, 47, '2020-10-22 12:02:48', '0', 'ST76');
INSERT INTO `meter_d` VALUES (16, 48, '2020-10-22 12:03:54', '85', 'CR76');
INSERT INTO `meter_d` VALUES (17, 49, '2020-10-22 12:08:08', '200', 'st');
INSERT INTO `meter_d` VALUES (18, 50, '2020-10-22 12:09:07', '0', 'st');
INSERT INTO `meter_d` VALUES (19, 51, '2020-10-22 12:09:09', '0', 'st');
INSERT INTO `meter_d` VALUES (20, 52, '2020-10-22 12:09:19', '85', 'CR');
INSERT INTO `meter_d` VALUES (21, 53, '2020-10-22 12:09:35', '85', 'CR');
INSERT INTO `meter_d` VALUES (22, 54, '2020-10-22 12:26:02', '0', 'ST76');
INSERT INTO `meter_d` VALUES (23, 55, '2020-10-22 13:59:21', '0', 'ST76');
INSERT INTO `meter_d` VALUES (24, 56, '2020-10-22 14:00:38', '0', 'ST76');
INSERT INTO `meter_d` VALUES (25, 57, '2020-10-22 14:02:56', '85', 'CR76');
INSERT INTO `meter_d` VALUES (26, 58, '2020-10-22 15:22:40', '0', '测试com1');
INSERT INTO `meter_d` VALUES (27, 62, '2020-10-30 10:04:29', '0', 'ST76');
INSERT INTO `meter_d` VALUES (28, 63, '2020-10-30 10:05:43', '0', 'CR76');
INSERT INTO `meter_d` VALUES (29, 63, '2020-11-02 14:23:57', '0', 'CR76');
INSERT INTO `meter_d` VALUES (30, 62, '2020-11-02 14:23:58', '0', 'ST76');
INSERT INTO `meter_d` VALUES (31, 64, '2020-11-10 10:40:04', '20003', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (32, 64, '2020-11-17 10:19:49', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (33, 64, '2020-11-23 15:15:46', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (34, 64, '2020-11-24 12:09:13', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (35, 64, '2020-11-25 10:40:11', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (36, 64, '2020-12-24 16:36:15', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (37, 65, '2020-12-29 14:49:25', '0', '00');
INSERT INTO `meter_d` VALUES (38, 66, '2020-12-29 14:54:30', '0', '02');
INSERT INTO `meter_d` VALUES (39, 67, '2020-12-29 15:08:33', '0', '02');
INSERT INTO `meter_d` VALUES (40, 64, '2020-12-29 19:00:19', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (41, 64, '2021-01-06 17:14:23', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (42, 65, '2021-01-06 17:14:23', '0', '00');
INSERT INTO `meter_d` VALUES (43, 65, '2021-03-30 18:59:46', '0', '00');
INSERT INTO `meter_d` VALUES (44, 64, '2021-03-30 18:59:46', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (45, 67, '2021-03-30 19:00:46', '0', '02');
INSERT INTO `meter_d` VALUES (46, 65, '2021-06-09 09:32:54', '0', '00');
INSERT INTO `meter_d` VALUES (47, 64, '2021-06-09 09:32:54', '0', '测试Com8ST76');
INSERT INTO `meter_d` VALUES (48, 67, '2021-06-09 16:03:03', '0', '02');

SET FOREIGN_KEY_CHECKS = 1;
