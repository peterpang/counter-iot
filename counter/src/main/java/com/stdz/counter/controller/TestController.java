package com.stdz.counter.controller;

import com.stdz.counter.service.MeterService;
import com.stdz.counter.service.ModbusTypeService;
import com.stdz.counter.utils.MeterValueUtil;
import de.re.easymodbus.modbusclient.ModbusClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

/**
 * @author peanut
 */


@RequestMapping("/test")
@Controller
public class TestController {

    @Autowired
    ModbusTypeService modbusTypeService;

    @Autowired
    MeterService meterService;

    @RequestMapping("index")
    public String getTest() {
        System.out.println("你好啊");
//        ModbusType modbusType = new ModbusType();
//        modbusType.setIp("COM3");
//        modbusType.setPort("502");
//        modbusType.setType(3);
//        modbusType.setCreateTime(new Date());
//        modbusType.setUpdateTime(new Date());
//        modbusTypeService.addModbus(modbusType);

          // 添加仪表
//        Meter meter = new Meter();
//        meter.setModbusId(1L);
//        meter.setName("测试");
//        meterService.addMeter(meter);
        return "index";
    }
}
