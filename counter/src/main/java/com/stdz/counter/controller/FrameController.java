package com.stdz.counter.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author peanut
 */

@RequestMapping("/frame")
@Controller
public class FrameController {

    @RequestMapping("index")
    public String getIndex() {
        return "index";
    }


    @RequestMapping("allCounter")
    public String getAllCounter() {
        return "allCounter";
    }

    @RequestMapping("diagram")
    public String getDiagram() {
        return "diagram";
    }

    @RequestMapping("histogram")
    public String getHistogram() {
        return "histogram";
    }

    @RequestMapping("details")
    public String getDetails() {
        return "details";
    }

    @RequestMapping("setCounter")
    public String getSetCounter() {
        return "setCounter";
    }

    @RequestMapping("communicationChoice")
    public String getCommunicationChoice() {
        return "communicationChoice";
    }

    @RequestMapping("meter")
    public String getMeter() {
        return "meter";
    }
}
