package com.stdz.counter.constant;

import com.stdz.counter.entity.MeterModbus;
import com.stdz.counter.utils.ReadSerialPortIdDataUtil;
import de.re.easymodbus.modbusclient.ModbusClient;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

/**
 * @author peanut
 */
public class MeterConstant {

    public static final Integer ERROR_PARAM_INT = -1;

    public static final String ERROR_PARAM_STR = "-1";

    public static final int BCD_REGISTER_ADDRESS = 1;

    public static final String CLEAR_MODE2 = "2";

    public static final String CLEAR_MODE1 = "1";

    public static final int INT_REGISTER_ADDRESS = 21;

    public static final String FLOAT_REGISTER_ADDRESS = "25";

    public static final int READ_METER_DIGIT = 4;

    public static final int UPPER_ROW_INDEX = 1;

    public static final int LOW_ROW_INDEX = 3;

    public static  final String BIT_ZERO_FLAG = "0";

    public static  final String BIT_ONE_FLAG = "1";

    public static final int BCD_METER_ALARM = 15;

    public static final int INT_METER_ALARM = 14;

    public static final int BCD_SCAL = 20;

    public static final int INT_SCAL = 18;

    /**
     * key为串口  value为数据库数据（list）
     */
    public static ConcurrentHashMap<String, List<MeterModbus>> serialPortMapCache = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, ReadSerialPortIdDataUtil> serialPortThreadMap = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, ModbusClient> modbusClientMap = new ConcurrentHashMap<>();

    // public static Map<String, ScheduledFuture<?>> taskFutures = new ConcurrentHashMap<String, ScheduledFuture<?>>();

    public static ConcurrentHashMap<String, Runnable> taskRunnableMap = new ConcurrentHashMap<>();

    public static ScheduledTaskRegistrar scheduledTaskRegistrar = new ScheduledTaskRegistrar();

}
