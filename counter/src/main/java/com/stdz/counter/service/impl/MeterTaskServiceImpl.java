package com.stdz.counter.service.impl;

import com.stdz.counter.dao.MeterTaskDao;
import com.stdz.counter.entity.MeterTask;
import com.stdz.counter.service.MeterTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author peanut
 */

@Service
public class MeterTaskServiceImpl implements MeterTaskService {

    @Autowired
    MeterTaskDao meterTaskDao;

    @Override
    public void addMeterTask(MeterTask meterTask) {
        meterTaskDao.addMeterTask(meterTask);
    }

    @Override
    public List<MeterTask> findAllCron() {
        return meterTaskDao.findAllCron();
    }

    @Override
    public MeterTask findByMeterId(Integer meterId) {
        return meterTaskDao.findByMeterId(meterId);
    }

    @Override
    public void updateByMeterId(Integer meterId) {
        meterTaskDao.updateByMeterId(meterId);
    }
}
