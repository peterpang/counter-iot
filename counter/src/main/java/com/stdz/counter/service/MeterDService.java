package com.stdz.counter.service;

import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface MeterDService {
    List<Map<String, Object>> findByDate(String startTime, String endTime);

}
