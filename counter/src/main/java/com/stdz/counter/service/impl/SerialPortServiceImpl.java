package com.stdz.counter.service.impl;

import com.stdz.counter.constant.MeterConstant;
import com.stdz.counter.dao.ModbusTypeDao;
import com.stdz.counter.entity.MeterModbus;
import com.stdz.counter.service.SerialPortService;
import com.stdz.counter.utils.ReadSerialPortIdDataUtil;
import com.stdz.counter.utils.SchedulingRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * @author peanut
 */
@Service
public class SerialPortServiceImpl implements SerialPortService {

    private static final Logger LOG = LoggerFactory.getLogger(SchedulingRunnable.class);

    private ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    @Autowired
    ModbusTypeDao modbusTypeDao;

    @Override
    public void readSerialPortIdData(String com) throws Exception {
        // 先更新全局map的数据
        // 查询数据库（通讯接口和仪表）
        List<MeterModbus> singleSerialPortList = modbusTypeDao.findAllLinkModbusType(com);
        MeterConstant.serialPortMapCache.put(com, singleSerialPortList);
        LOG.debug("更新串口: {}完成", com);

        ReadSerialPortIdDataUtil readSerialPortIdDataUtil = new ReadSerialPortIdDataUtil(com);
        cachedThreadPool.submit(readSerialPortIdDataUtil);
    }

}


