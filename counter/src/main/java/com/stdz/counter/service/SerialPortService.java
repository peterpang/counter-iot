package com.stdz.counter.service;

/**
 * 串口读取数据的接口
 * @author peanut
 */
public interface SerialPortService {

    /**
     * 重启串口
     * @param com
     * @throws Exception
     */
    void readSerialPortIdData(String com) throws Exception;
}
