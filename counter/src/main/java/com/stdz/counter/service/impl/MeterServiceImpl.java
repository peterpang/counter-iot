package com.stdz.counter.service.impl;

import com.stdz.counter.dao.MeterDao;
import com.stdz.counter.dao.MeterRecordDao;
import com.stdz.counter.entity.Meter;
import com.stdz.counter.entity.MeterRecord;
import com.stdz.counter.service.MeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @author peanut
 */
@Service
public class MeterServiceImpl implements MeterService {

    @Autowired
    MeterDao meterDao;

    @Autowired
    MeterRecordDao meterRecordDao;



    @Override
    public void addMeter(Meter meter) {
        meterDao.addMeter(meter);
    }

    @Override
    public void updateValues(Long id, String upperRow, String lowerRow, String Al1, String Al2, String scal, String offLine) {
        meterDao.updateValues(id, upperRow, lowerRow, Al1, Al2, scal, offLine);
    }

    @Override
    public MeterRecord findById(Integer mId) {
        return meterRecordDao.findById(mId);
    }

    @Override
    public Integer findByCom(Map map) {
        return meterDao.findByCom(map);
    }

    @Override
    public List<Meter> findAlls() {
        return meterDao.findAlls();
    }
}
