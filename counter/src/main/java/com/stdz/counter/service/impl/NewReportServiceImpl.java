package com.stdz.counter.service.impl;


import cn.hutool.json.JSONArray;
import com.stdz.counter.dao.*;
import com.stdz.counter.entity.*;
import com.stdz.counter.service.ReportService;
import com.stdz.counter.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author peanut
 */
@Service("newReportService")
@Slf4j
public class NewReportServiceImpl implements ReportService {

    private static final Logger LOG = LoggerFactory.getLogger(NewReportServiceImpl.class);
    @Autowired
    private MeterInfoDao meterInfoDao;
    @Autowired
    private MeterRecordDao meterRecordDao;
    @Autowired
    private MeterMDao meterMDao;
    @Autowired
    private MeterHDao meterHDao;
    @Autowired
    private MeterDDao meterDDao;
    @Autowired
    private MeterYDao meterYDao;

    public List<CurveData> findByDate(String type, String date, String meterIds) throws ParseException {
        List<MeterRecord> meterRecords = new ArrayList<>();
        List<CurveData> list = new ArrayList<>();
        String endTime = null;
        String[] meterIdClt = meterIds.split(",");
        switch (type){
            case "1":
                this.handleHourData(date, meterIdClt, list);
                break;
            case "2":

                this.handleDayData(date, meterIdClt, list);
                break;
            case "3":
                this.handleMonthData(date, meterIdClt, list);
                break;
            case "4":
                this.handleYearData(date, meterIdClt, list);
                break;
            default:
                this.handleMinuteData(date, meterIdClt, list);
                break;
        }
        return list;
    }

    private void handleHourData(String date, String[] meterIdClt, List<CurveData> dataList) throws ParseException {
        // 后一个小时
        String endTime = DateUtils.formatDateLastHour(DateUtils.getLaterHour(date));
        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);
            CurveData curveData = new CurveData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();
            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            List<MeterRecord> meterRecords = meterMDao.queryMinuteDataByHour(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    curveData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
//                        if (i == 0) {
//                            JSONArray data = new JSONArray();
//                            als.add(BigDecimal.ZERO);
//                        } else {
//                            als.add(als.get(i - 1));
//                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.MINUTE));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));

                    }
                }
                curveData.setData(datas);
//                curveData.setAreaStyle("");
                curveData.setStack("个数");
                curveData.setType("line");
                dataList.add(curveData);
            }
        }
    }

    private void handleDayData(String date, String[] meterIdClt, List<CurveData> dataList) throws ParseException {
        // 后一天
        String endTime = DateUtils.formatDateLastDay(DateUtils.getLaterDay(date));
        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);

            CurveData curveData = new CurveData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            JSONArray datas = new JSONArray();

            List<MeterRecord> meterRecords = meterHDao.queryHourDataByDate(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    curveData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.HOUR_OF_DAY));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                curveData.setData(datas);
//                curveData.setAreaStyle("");
                curveData.setStack("个数");
                curveData.setType("line");
                dataList.add(curveData);
            }
        }
    }

    private void handleMonthData(String date, String[] meterIdClt, List<CurveData> dataList) {
        // 后一月
        String[] dateStr = date.split("-");
        String year = dateStr[0];
        String laterMonth = "";
        if (dateStr.equals("12")) {
            laterMonth = "01";
            year  = String.valueOf(Integer.valueOf(year) + 1);
        } else {
            laterMonth = String.valueOf(Integer.valueOf(dateStr[1]) + 1);
        }

        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);

            CurveData curveData = new CurveData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            if (laterMonth.length() < 2) {
                laterMonth = "0" + laterMonth;
            }
            List<MeterRecord> meterRecords = meterDDao.queryDayDataByMonth(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    curveData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.DAY_OF_MONTH));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                curveData.setData(datas);
//                curveData.setAreaStyle("");
                curveData.setStack("个数");
                curveData.setType("line");
                dataList.add(curveData);
            }
        }
    }

    private void handleYearData(String date, String[] meterIdClt, List<CurveData> dataList) {
        // 后一年
        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);
            CurveData curveData = new CurveData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            List<MeterRecord> meterRecords = meterYDao.queryMonthDataByYear(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    curveData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.MONTH));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                curveData.setData(datas);
//                curveData.setAreaStyle("");
                curveData.setStack("个数");
                curveData.setType("line");
                dataList.add(curveData);
            }
        }
    }

    private void handleMinuteData(String date, String[] meterIdClt, List<CurveData> dataList) throws ParseException {
        // 前一分钟(获取年月日时分)
        // 后一分钟
        String endTime = DateUtils.formatDateLastTime(DateUtils.getLaterMinute(date));

        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);
            CurveData curveData = new CurveData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);

            List<MeterRecord> meterRecords = meterRecordDao.querySecondDataByMinute(iMeterId, date);

            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    curveData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.SECOND));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                curveData.setData(datas);
//                curveData.setAreaStyle("");
                curveData.setStack("个数");
                curveData.setType("line");
                dataList.add(curveData);
            }
        }
    }

    public List<ColumnarData> findColumnChartByDate(String type, String date, String meterIds) throws ParseException {
        List<ColumnarData> list = new ArrayList<>();
        String[] meterIdClt = meterIds.split(",");
        switch (type){
            case "1":
                this.handleHourColumnData(date, meterIdClt, list);
                break;
            case "2":
                this.handleDayColumnData(date, meterIdClt, list);
                break;
            case "3":
                this.handleMonthColumnData(date, meterIdClt, list);
                break;
            case "4":
                this.handleYearColumnData(date, meterIdClt, list);
                break;
            default:
                this.handleMinuteColumnData(date, meterIdClt, list);
                break;
        }
        return list;
    }

    private void handleHourColumnData(String date, String[] meterIdClt, List<ColumnarData> dataList) throws ParseException {
        // 后一个小时
        String endTime = DateUtils.formatDateLastHour(DateUtils.getLaterHour(date));
        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);
            ColumnarData columnarData = new ColumnarData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            List<MeterRecord> meterRecords = meterMDao.queryMinuteDataByHour(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    columnarData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.MINUTE));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                columnarData.setData(datas);
                columnarData.setType("bar");
                columnarData.setLabel(new LabelOption());
                dataList.add(columnarData);
            }
        }
    }

    private void handleDayColumnData(String date, String[] meterIdClt, List<ColumnarData> dataList) throws ParseException {

        // 后一天
        String endTime = DateUtils.formatDateLastDay(DateUtils.getLaterDay(date));
        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);
            ColumnarData columnarData = new ColumnarData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            List<MeterRecord>  meterRecords = meterHDao.queryHourDataByDate(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    columnarData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.HOUR_OF_DAY));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                columnarData.setData(datas);
                columnarData.setType("bar");
                columnarData.setLabel(new LabelOption());
                dataList.add(columnarData);
            }
        }
    }

    private void handleMonthColumnData(String date, String[] meterIdClt, List<ColumnarData> dataList) throws ParseException {
        // 后一月
        String[] dateStr = date.split("-");
        String year = dateStr[0];
        String laterMonth = "";
        if (dateStr.equals("12")) {
            laterMonth = "01";
            year  = String.valueOf(Integer.valueOf(year) + 1);
        } else {
            laterMonth = String.valueOf(Integer.valueOf(dateStr[1]) + 1);
        }

        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);

            ColumnarData columnarData = new ColumnarData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            if (laterMonth.length() < 2) {
                laterMonth = "0" + laterMonth;
            }
            List<MeterRecord> meterRecords = meterDDao.queryDayDataByMonth(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    columnarData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.DAY_OF_MONTH));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                columnarData.setData(datas);
                columnarData.setType("bar");
                columnarData.setLabel(new LabelOption());
                dataList.add(columnarData);
            }
        }
    }

    private void handleYearColumnData(String date, String[] meterIdClt, List<ColumnarData> dataList) throws ParseException {
        // 后一年
        // map = meterDDao.findByDate(date, String.valueOf(Integer.valueOf(date) + 1));
        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);
            ColumnarData columnarData = new ColumnarData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();

            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            List<MeterRecord> meterRecords = meterYDao.queryMonthDataByYear(iMeterId, date);
            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    columnarData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.MONTH));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                columnarData.setData(datas);
                columnarData.setType("bar");
                columnarData.setLabel(new LabelOption());
                dataList.add(columnarData);
            }
        }
    }

    private void handleMinuteColumnData(String date, String[] meterIdClt, List<ColumnarData> dataList) throws ParseException {
        // 前一分钟(获取年月日时分)
        // 后一分钟
        String endTime = DateUtils.formatDateLastTime(DateUtils.getLaterMinute(date));

        for (String meterId : meterIdClt) {
            int iMeterId = Integer.parseInt(meterId);

            ColumnarData columnarData = new ColumnarData();
            // al1的数组
            List<BigDecimal> als = new ArrayList<>();
            JSONArray datas = new JSONArray();


            MeterInfo meterInfo = meterInfoDao.queryMeterInfo(iMeterId);
            List<MeterRecord> meterRecords = meterRecordDao.findByDate(date, endTime, iMeterId);

            if (meterRecords != null && meterRecords.size() > 0) {
                for (int i = 0; i < meterRecords.size(); i++ ) {
                    MeterRecord meterRecord = meterRecords.get(i);
                    columnarData.setName(meterInfo.getMeterName());
                    if (meterRecord == null || StringUtils.isEmpty(meterRecord.getUpperRow())) {
                        if (i == 0) {
                            als.add(BigDecimal.ZERO);
                        } else {
                            als.add(als.get(i - 1));
                        }
                    } else {
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                        JSONArray data = new JSONArray();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(meterRecord.getCreateTime().getTime());
                        data.add(calendar.get(Calendar.SECOND));
                        data.add(new BigDecimal(meterRecord.getUpperRow()));
                        datas.add(data);
                        als.add(new BigDecimal(meterRecord.getUpperRow()));
                    }
                }
                columnarData.setData(datas);
                columnarData.setType("bar");
                columnarData.setLabel(new LabelOption());
                dataList.add(columnarData);
            }
        }
    }
}
