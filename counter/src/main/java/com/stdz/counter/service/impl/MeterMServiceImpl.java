package com.stdz.counter.service.impl;

import com.stdz.counter.dao.MeterMDao;
import com.stdz.counter.dao.MeterRecordDao;
import com.stdz.counter.entity.MeterRecord;
import com.stdz.counter.service.MeterMService;
import com.stdz.counter.service.MeterRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @author peanut
 */
@Service
public class MeterMServiceImpl implements MeterMService {


    @Autowired
    MeterMDao meterMDao;


    @Override
    public List<MeterRecord> findByDate(String startTime, String endTime, Integer meterId) {
        return meterMDao.findByDate(startTime, endTime, meterId);
    }
}
