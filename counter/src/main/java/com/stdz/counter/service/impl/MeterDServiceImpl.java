package com.stdz.counter.service.impl;

import com.stdz.counter.dao.MeterDDao;
import com.stdz.counter.dao.MeterMDao;
import com.stdz.counter.entity.MeterRecord;
import com.stdz.counter.service.MeterMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @author peanut
 */
@Service
public class MeterDServiceImpl implements MeterMService {


    @Autowired
    MeterDDao meterDDao;


    @Override
    public List<MeterRecord> findByDate(String startTime, String endTime, Integer meterId) {
        return meterDDao.findByDate(startTime, endTime, meterId);
    }
}
