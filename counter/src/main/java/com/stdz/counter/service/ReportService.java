package com.stdz.counter.service;

import com.stdz.counter.entity.ColumnarData;
import com.stdz.counter.entity.CurveData;

import java.text.ParseException;
import java.util.List;

/**
 * @author xiaodou
 * @date 2020-09-12 13:04
 */
public interface ReportService {

    List<CurveData> findByDate(String type, String date, String meterIds) throws ParseException;

    List<ColumnarData> findColumnChartByDate(String type, String date, String meterIds) throws ParseException;
}
