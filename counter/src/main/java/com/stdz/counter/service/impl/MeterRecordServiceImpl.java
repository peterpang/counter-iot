package com.stdz.counter.service.impl;

import com.stdz.counter.dao.MeterRecordDao;
import com.stdz.counter.entity.MeterRecord;
import com.stdz.counter.service.MeterRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author peanut
 */
@Service
public class MeterRecordServiceImpl implements MeterRecordService {


    @Autowired
    MeterRecordDao meterRecordDao;


    @Override
    public void addMeterRecord(MeterRecord meterRecord) {
        meterRecordDao.addMeterRecord(meterRecord);
    }

    @Override
    public List<MeterRecord> findByDate(String date, String endTime, Integer meterId) {
        return meterRecordDao.findByDate(date, endTime, meterId);
    }
}
