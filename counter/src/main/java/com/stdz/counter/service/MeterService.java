package com.stdz.counter.service;

import com.stdz.counter.entity.Meter;
import com.stdz.counter.entity.MeterRecord;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface MeterService {

    /**
     * 添加仪表
     * @param meter
     */
    void addMeter(Meter meter);

    /**
     * 修改仪表的数值
     * @param id
     * @param upperRow
     * @param lowerRow
     */
    void updateValues(Long id, String upperRow, String lowerRow, String Al1, String Al2, String scal, String offLine);

    MeterRecord findById(Integer mId);

    Integer findByCom(Map map);

    List<Meter> findAlls();
}
