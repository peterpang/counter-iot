package com.stdz.counter.service;

import com.stdz.counter.entity.MeterRecord;

import java.util.List;

/**
 * @author peanut
 */
public interface MeterRecordService {

    void addMeterRecord(MeterRecord meterRecord);

    List<MeterRecord> findByDate(String date, String endTime, Integer meterId);
}
