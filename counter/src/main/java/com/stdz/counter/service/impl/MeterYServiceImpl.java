package com.stdz.counter.service.impl;

import com.stdz.counter.dao.MeterMDao;
import com.stdz.counter.dao.MeterYDao;
import com.stdz.counter.entity.MeterRecord;
import com.stdz.counter.service.MeterMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author peanut
 */
@Service
public class MeterYServiceImpl implements MeterMService {


    @Autowired
    MeterYDao meterYDao;


    @Override
    public List<MeterRecord> findByDate(String startTime, String endTime, Integer meterId) {
        return meterYDao.findByDate(startTime, endTime, meterId);
    }
}
