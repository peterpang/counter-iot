package com.stdz.counter.service;

import com.stdz.counter.entity.ModbusType;

import java.util.List;

/**
 * @author peanut
 */
public interface ModbusTypeService {

    void addModbus(ModbusType modbusType);

    List<String> findSerialPorts();
}
