package com.stdz.counter.service;

import com.stdz.counter.entity.MeterRecord;

import java.util.List;

/**
 * @author peanut
 */
public interface MeterHService {
    List<MeterRecord> findByDate(String startTime, String endTime, Integer meterId);

}
