package com.stdz.counter.service;

import com.stdz.counter.entity.MeterTask;

import java.util.List;

/**
 * @author peanut
 */
public interface MeterTaskService {

    void addMeterTask(MeterTask meterTask);

    List<MeterTask> findAllCron();

    MeterTask findByMeterId(Integer meterId);

    void updateByMeterId(Integer meterId);
}
