package com.stdz.counter.service;

import com.github.pagehelper.Page;
import com.stdz.counter.entity.ColumnarData;
import com.stdz.counter.entity.CurveData;
import com.stdz.counter.entity.ExcelTemplate;
import com.stdz.counter.entity.Meter;
import de.re.easymodbus.exceptions.ModbusException;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface DeviceService {

    /**
     * 添加设备
     * @param map
     * @return
     * @throws Exception
     */
    Boolean addDevice(Map map) throws Exception;

    /**
     * 重启串口
     * @param serialPort
     * @return
     */
    Boolean restartSerialPort(String serialPort);

    /**
     * 获取到所有的计数器
     * @return
     */
    Page<Meter> findAllCounters();

    /**
     * 获取到所有的串口
     * @return
     */
    List<String> findAllPorts();

    /**
     * 暂停设备
     * @param id
     */
    void suspend(String com, int id) throws Exception;

    /**
     * 通过仪表id去获取信息（包括通讯接口）
     * @param id
     * @return
     */
    Map findById(Long id);

    /**
     * 清零
     * @param port
     * @param ip
     * @param clearMode
     * @param flag
     */
    void clear(String port, int ip, String clearMode, String flag) throws Exception;

    /**
     * 通过id去删除仪表
     * @param id
     */
    void delete(Long id);

    void setCounter(Map map) throws IOException, ModbusException;

    List<String> findAllComs();

    List<Map<String, Object>> findAllModbusType();

    void clear(String id) throws Exception;

    List<CurveData> findByDate(String type, String date, String meterIds) throws ParseException;

    List<ColumnarData> findColumnChartByDate(String type, String date, String meterIds) throws ParseException;

    Long findByOnLine();

    List<ExcelTemplate> findByDay(String type, String date,String laterDate, String meterIds);

    Map<String, List<ExcelTemplate>> groupLists(List<ExcelTemplate> lists);

    List<Meter> findAll();
}
