package com.stdz.counter.service.impl;

import com.stdz.counter.dao.ModbusTypeDao;
import com.stdz.counter.entity.ModbusType;
import com.stdz.counter.service.ModbusTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author peanut
 */
@Service
public class ModbusTypeServiceImpl implements ModbusTypeService {

    @Autowired
    private ModbusTypeDao modbusTypeDao;

    @Override
    public void addModbus(ModbusType modbusType) {
        modbusTypeDao.addModbus(modbusType);
    }

    @Override
    public List<String> findSerialPorts() {
        return modbusTypeDao.findSerialPorts();
    }
}
