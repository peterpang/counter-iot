package com.stdz.counter.task;


import com.stdz.counter.service.DeviceService;
import com.stdz.counter.service.ModbusTypeService;
import com.stdz.counter.service.SerialPortService;
import com.stdz.counter.utils.ReadSerialPortIdDataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author peanut
 */
// @Component
public class UpdateOffLine {

    private static final Logger LOG = LoggerFactory.getLogger(ReadSerialPortIdDataUtil.class);

    @Autowired
    SerialPortService serialPortService;

    @Autowired
    ModbusTypeService modbusTypeService;

    @Autowired
    DeviceService deviceService;

    // @Scheduled(cron = "0 0/1 * * * *")
    public void timer() throws Exception {
//        LOG.debug("UpdateOffLine开始进入定时器.......");
//        List<String> serialPorts = modbusTypeService.findSerialPorts();
//        LOG.debug("UpdateOffLine串口查询成功......." + serialPorts.size());
//        if (serialPorts.size() > 0 && serialPorts != null) {
//            for (String serialPort : serialPorts) {
//                LOG.debug("当前的串口为：{}" , serialPort);
//                serialPortService.readSerialPortIdData(serialPort);
//            }
//        }
    }
}
