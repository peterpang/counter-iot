package com.stdz.counter.handler;

public abstract class IDataHandler {

    public abstract void handle();
}
