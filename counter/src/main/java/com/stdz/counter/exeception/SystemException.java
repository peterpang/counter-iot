package com.stdz.counter.exeception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author peanut
 */
@RestControllerAdvice
public class SystemException {

    private static final Logger logger = LoggerFactory.getLogger(SystemException.class);

    @ExceptionHandler(value=Exception.class)
    Object handleException(Exception e, HttpServletRequest request){
         logger.error("url {}, msg {}",request.getRequestURL(), e.getMessage());
         Map<String, Object> map = new HashMap<>();
         map.put("code", 0);
         map.put("msg", e.getMessage());
         map.put("url", request.getRequestURL());
         return map;
    }
}
