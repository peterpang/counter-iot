package com.stdz.counter.utils;

import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetTime;
import java.util.Date;
import java.util.Objects;

/**
 * @author peanut
 */
public class DateUtils {

    private static final SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final SimpleDateFormat SDF_DATE_FMT = new SimpleDateFormat("ss mm HH dd MM ?");

    private static final SimpleDateFormat SDF_DATE_YMDHM = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private static final SimpleDateFormat SDF_DATE_YMDH = new SimpleDateFormat("yyyy-MM-dd HH");

    private static final SimpleDateFormat SDF_DATE_YMD = new SimpleDateFormat("yyyy-MM-dd");

    private static final int SECOND = 1000;



    /**
     * 功能描述：日期转换cron表达式
     * @param date
     * @return
     */
    public static String formatDateByPattern(Date date) {
        String formatTimeStr = null;
        if (Objects.nonNull(date)) {
            formatTimeStr = SDF_DATE_FMT.format(date);
        }
        return formatTimeStr;
    }

    /**
     * 将时间转化为cron表达式
     * @param date 日期
     * @return
     */
    public static String getCron(Date date) {
        return formatDateByPattern(date);
    }

    /**
     * 讲字符串转为Date
     * @param dateStr
     * @return
     */
    public static Date getDate(String dateStr) {
        return DateUtil.parse(dateStr);
    }

    /**
     * 将具体的某个时间转为表达式
     * @param date
     * @return
     */
    public static String getCron(String date) {
        StringBuilder formatTimeStr = new StringBuilder();
        if (Objects.nonNull(date)) {
            // 时分秒数组
            String[] HMM = date.split(":");
            // "30 10 1 * * ?"(示例)
            formatTimeStr = formatTimeStr.append(HMM[2]).append(" ")
                    .append(HMM[1]).append(" ").append(HMM[0]).append(" * * ?");
        }
        return formatTimeStr.toString();
    }


    /**
     * 将cron表达式转为时间格式
     * @param cron
     * @return
     */
        public static String getCronCoverDate(String cron) {
        StringBuilder formatTimeStr = new StringBuilder();
        String[] crons = cron.split(" ");

        if (!crons[4].equals("*") && !crons[3].equals("*")) {
            formatTimeStr.append("2020").append(":");
            formatTimeStr.append(crons[4])
                    .append(":").append(crons[3]).append(":");
        }
        if (!crons[2].equals("*")) {
            formatTimeStr.append(crons[2]).append(":");
        }
        if (!crons[1].equals("*")) {
            formatTimeStr.append(crons[1]).append(":");
        }
        if (!crons[0].equals("*")) {
            formatTimeStr.append(crons[0]);
        }
        return formatTimeStr.toString();
    }

    public static long getLastMinute(String date) throws ParseException {
        Date nowDate = SDF_DATE_YMDHM.parse(date);
        return nowDate.getTime() - 60 * 1000;
    }

    public static long getLaterMinute(String date) throws ParseException {
        return SDF_DATE_YMDHM.parse(date).getTime() + 60 * SECOND;
    }

    public static long getLaterHour(String date) throws ParseException {
        return SDF_DATE_YMDH.parse(date).getTime() + 60 * SECOND * 60;
    }

    public static long getNowYMDH(String date) throws ParseException {
        return SDF_DATE_YMDH.parse(date).getTime();
    }

    public static long getNowYMD(String date) throws ParseException {
        return SDF_DATE_YMD.parse(date).getTime();
    }

    public static long getLaterDay(String date) throws ParseException {
        return SDF_DATE_YMD.parse(date).getTime() + 60 * SECOND * 60 * 24;
    }

    public static String formateDate(Date date) throws ParseException {
        return SDF_DATE.format(date);
    }

    /**
     *
     * @param time
     * @return
     */
    public static String formatDateLastTime(long time) {
        return SDF_DATE_YMDHM.format(time);
    }

    public static String formatDateLastHour(long time) {
        return SDF_DATE_YMDH.format(time);
    }

    public static String formatDateLastDay(long time) {
        return SDF_DATE_YMD.format(time);
    }

    public static int getSecond(int multiple) {
        return SECOND * multiple;
    }


    public static String timeStamp2Date(String seconds,String format) {
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
            return "";
        }
        if(format == null || format.isEmpty()){
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds+"000")));
    }


}
