package com.stdz.counter.utils;

import java.util.ArrayList;

public class PageList<E> extends ArrayList<E> {

    private static final long serialVersionUID = 8172768353827243795L;
    
    /**
     * 页码
     */
    private int pageNum;
    
    /**
     * 每页记录数
     */
    private int pageSize;
    /**
     * 总共记录数
     */
    private int rows;
    /**
     * 分页下标
     */
    private int limitIndex;
    /**
     * 导航条固定显示多少页
     */
    private int navigatePages;
    
    public PageList(int pageNum, int pageSize, int rows) {
        super();
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.rows = rows;
    }

    public PageList(int pageNum, int pageSize, int rows, int navigatePages) {
        super();
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.rows = rows;
        this.limitIndex = (this.pageNum * this.pageSize)-(this.pageSize-1);
        this.navigatePages = navigatePages;
    }

    public int getPageNum() {
        return pageNum;
    }
    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    public int getRows() {
        return rows;
    }
    public void setRows(int rows) {
        this.rows = rows;
    }
    public int getNavigatePages() {
        return navigatePages;
    }
    public void setNavigatePages(int navigatePages) {
        this.navigatePages = navigatePages;
    }

    public int getLimitIndex() {
        return limitIndex;
    }

    public void setLimitIndex(int limitIndex) {
        this.limitIndex = limitIndex;
    }
}