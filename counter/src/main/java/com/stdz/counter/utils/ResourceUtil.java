package com.stdz.counter.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import java.lang.reflect.Field;

public class ResourceUtil {
    
    private static transient Logger logger = LoggerFactory.getLogger(ResourceUtil.class);
    
    private static ApplicationContext context;
    
    public static void init(ApplicationContext context) {
        ResourceUtil.context = context;
    }
    
    public static ApplicationContext getContext() {
        return context;
    }

    public static <T> void autowired(Class<T> clazz, T t) {
        Field[] fields = clazz.getDeclaredFields();
        if (fields != null && fields.length > 0) {
            for (Field field : fields) {
                if (field != null) {
                    Resource resource = field.getAnnotation(Resource.class);
                    if (resource != null) {
                        field.setAccessible(true);
                        Object object = context.getBean(field.getType());
                        if (object != null) {
                            try {
                                field.set(t, object);
                            } catch (IllegalArgumentException e) {
                                logger.error("ILogic exception:", e);
                            } catch (IllegalAccessException e) {
                                logger.error("ILogic exception:", e);
                            }
                            continue;
                        }
                        if (StringUtils.isNotBlank(resource.name())) {
                            if (context.containsBean(resource.name())) {
                                try {
                                    field.set(t, context.getBean(resource.name()));
                                } catch (BeansException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalArgumentException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalAccessException e) {
                                    logger.error("ILogic exception:", e);
                                }
                            } else if (context.containsBean(field.getName())) {
                                try {
                                    field.set(t, context.getBean(field.getName()));
                                } catch (BeansException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalArgumentException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalAccessException e) {
                                    logger.error("ILogic exception:", e);
                                }
                            }
                        } else {
                            if (context.containsBean(field.getName())) {
                                try {
                                    field.set(t, context.getBean(field.getName()));
                                } catch (BeansException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalArgumentException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalAccessException e) {
                                    logger.error("ILogic exception:", e);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static <T> void autowired(T t) {
        Field[] fields = t.getClass().getDeclaredFields();
        if (fields != null && fields.length > 0) {
            for (Field field : fields) {
                if (field != null) {
                    Resource resource = field.getAnnotation(Resource.class);
                    if (resource != null) {
                        field.setAccessible(true);
                        Object object = context.getBean(field.getType());
                        if (object != null) {
                            try {
                                field.set(t, object);
                            } catch (IllegalArgumentException e) {
                                logger.error("ILogic exception:", e);
                            } catch (IllegalAccessException e) {
                                logger.error("ILogic exception:", e);
                            }
                            continue;
                        }
                        if (StringUtils.isNotBlank(resource.name())) {
                            if (context.containsBean(resource.name())) {
                                try {
                                    field.set(t, context.getBean(resource.name()));
                                } catch (BeansException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalArgumentException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalAccessException e) {
                                    logger.error("ILogic exception:", e);
                                }
                            } else if (context.containsBean(field.getName())) {
                                try {
                                    field.set(t, context.getBean(field.getName()));
                                } catch (BeansException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalArgumentException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalAccessException e) {
                                    logger.error("ILogic exception:", e);
                                }
                            }
                        } else {
                            if (context.containsBean(field.getName())) {
                                try {
                                    field.set(t, context.getBean(field.getName()));
                                } catch (BeansException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalArgumentException e) {
                                    logger.error("ILogic exception:", e);
                                } catch (IllegalAccessException e) {
                                    logger.error("ILogic exception:", e);
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
}
