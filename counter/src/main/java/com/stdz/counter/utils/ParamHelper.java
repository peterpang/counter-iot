package com.stdz.counter.utils;

import com.stdz.counter.entity.Param;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParamHelper {

    private static Map<String, String> params = new HashMap<String, String>();

    public static void reloadParams(List<Param> tempParams) {
        if (tempParams != null && tempParams.size() > 0) {
            for (Param param : tempParams) {
                if (param != null && StringUtils.isNotBlank(param.getParamKey())) {
                    params.put(param.getParamKey(), param.getParamValue());
                }
            }
        }
    }

    public static String getParamVal(String key) {
        if (params == null || !params.containsKey(key)) {
            return null;
        } else {
            String value = params.get(key);
            return value;
        }
    }

    public static int getParamIntVal(String key, int defaultValue) {
        String value = getParamVal(key);
        if (StringUtils.isBlank(value)) {
            return defaultValue;
        }
        try {
            int intVal = Integer.parseInt(value.trim());
            return intVal;
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}
