package com.stdz.counter.utils;

/**
 * @author peanut
 */
public class ConversionUtil {

    private static final int SIXTEEN = 16;

    /**
     * 转为10进制
     * @param data
     * @return
     */
    public static Long toHexAdecimal(String data) {
        return Long.parseLong(data, SIXTEEN);
    }

    /**
     * 转为16进制
     * @param data
     * @return
     */
    public static String toTenAdecimal(String data) {
        return Integer.toHexString(Integer.parseInt(data));
    }


}
