package com.stdz.counter.utils;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 * @author peanut
 * @param <T>用于向前台返还List所用
 */
public class PageInfo<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 当前页码数
     */
    private int pageNum;
    /**
     * 每页显示的记录数
     */
    private int pageSize;
    /**
     * 开始行号
     */
    private long startRow;
    /**
     * 结束行号
     */
    private long endRow;
    /**
     *  总记录数
     */
    private long rows;
    /**
     *  总页码数
     */
    private int pages;
    /**
     *  导航条 页码数
     */
    private int navigatePages;
    /**
     * 导航条 具体页码数
     */
    private int[] navigatePageNums;
    /**
     *  导航条上的开始页
     */
    private int navigateStartPage;
    /**
     *  导航条上的结束页
     */
    private int navigateEndPage;
    /**
     *  首页页码
     */
    private int firstPage;
    /**
     *  上一页页码
     */
    private int previousPage;
    /**
     *  下一页页码
     */
    private int nextPage;
    /**
     *  尾页页码
     */
    private int lastPage;
    /**
     *  是否有上一页
     */
    private boolean hasPreviousPage = false;
    /**
     *  是否有下一页
     */
    private boolean hasNextPage = false;
    /**
     * 是否为首页
     */
    private boolean isFirstPage = false;
    /**
     * 是否为最后一页
     */
    private boolean isLastPage = false;

    // 存放本次分页查询出来的数据
    private List<T> list;

    /**
     *
     * @param list
     */
    public PageInfo(List<T> list) {
        if (list instanceof PageList) {
            PageList<T> pageList = (PageList<T>)list;
            // 当前页码
            this.pageNum = pageList.getPageNum();
            // 每页显示的记录数
            this.pageSize = pageList.getPageSize();
            // 总记录数
            this.rows = pageList.getRows();
            // 导航条 页码数
            this.navigatePages = pageList.getNavigatePages();
            // 总页码数
            this.pages = (int) Math.ceil(this.rows * 1.0 / this.pageSize);
            // 当前页码大于总页码数时，当前页码=总页码数
            if (this.pageNum > this.pages) {
                this.pageNum = this.pages;
            }
            // 开始行号
            this.startRow = (this.pageNum * this.pageSize)-(this.pageSize-1);
            // 结束行号
            this.endRow = this.pageNum * this.pageSize;
            //当结束行号>总行数时，结束行号=总行数
            if(this.endRow > rows) {
                this.endRow = rows;
            }
            // 导航条 开始页码和结束页码
            this.navigateStartPage = (this.pageNum - this.navigatePages / 2) > 0 ? (this.pageNum - this.navigatePages / 2) : 1;
            this.navigateEndPage = this.navigateStartPage+this.navigatePages;
            if(this.navigateEndPage>this.pages){
                this.navigateEndPage = this.pages;
                if(this.navigateEndPage>this.navigatePages){
                    this.navigateStartPage = this.navigateEndPage-this.navigatePages+1;
                }
            }
            //导航条具体页数
            this.navigatePageNums = new int[this.navigatePages];
            for(int i=this.navigateStartPage; i<=this.navigateEndPage; i++){
                this.navigatePageNums[i-this.navigateStartPage] = i;
            }

            //设置首页
            this.firstPage = 1;
            //设置尾页
            this.lastPage = this.pages;
            //是否为第一页
            this.isFirstPage = this.pageNum==this.firstPage;
            //是否为最后一页
            this.isLastPage = this.pageNum==this.lastPage;
            // 设置上一页
            if (this.pageNum > 1) {
                this.hasPreviousPage = true;
                this.previousPage = this.pageNum - 1;
            }
            // 设置下一页
            if (this.pageNum < this.pages) {
                this.hasNextPage = true;
                this.nextPage = this.pageNum + 1;
            }
        }
    }

    public int getPageNum() {
        return pageNum;
    }


    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }


    public int getPageSize() {
        return pageSize;
    }


    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


    public long getStartRow() {
        return startRow;
    }


    public void setStartRow(long startRow) {
        this.startRow = startRow;
    }


    public long getEndRow() {
        return endRow;
    }


    public void setEndRow(long endRow) {
        this.endRow = endRow;
    }


    public long getRows() {
        return rows;
    }


    public void setRows(long rows) {
        this.rows = rows;
    }


    public int getPages() {
        return pages;
    }


    public void setPages(int pages) {
        this.pages = pages;
    }


    public int getNavigatePages() {
        return navigatePages;
    }


    public void setNavigatePages(int navigatePages) {
        this.navigatePages = navigatePages;
    }


    public int[] getNavigatePageNums() {
        return navigatePageNums;
    }


    public void setNavigatePageNums(int[] navigatePageNums) {
        this.navigatePageNums = navigatePageNums;
    }


    public int getNavigateStartPage() {
        return navigateStartPage;
    }


    public void setNavigateStartPage(int navigateStartPage) {
        this.navigateStartPage = navigateStartPage;
    }


    public int getNavigateEndPage() {
        return navigateEndPage;
    }


    public void setNavigateEndPage(int navigateEndPage) {
        this.navigateEndPage = navigateEndPage;
    }


    public int getFirstPage() {
        return firstPage;
    }


    public void setFirstPage(int firstPage) {
        this.firstPage = firstPage;
    }


    public int getPreviousPage() {
        return previousPage;
    }


    public void setPreviousPage(int previousPage) {
        this.previousPage = previousPage;
    }


    public int getNextPage() {
        return nextPage;
    }


    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }


    public int getLastPage() {
        return lastPage;
    }


    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }


    public boolean isHasPreviousPage() {
        return hasPreviousPage;
    }


    public void setHasPreviousPage(boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }


    public boolean isHasNextPage() {
        return hasNextPage;
    }


    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public boolean isFirstPage() {
        return isFirstPage;
    }

    public void setFirstPage(boolean isFirstPage) {
        this.isFirstPage = isFirstPage;
    }

    public boolean isLastPage() {
        return isLastPage;
    }


    public void setLastPage(boolean isLastPage) {
        this.isLastPage = isLastPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
