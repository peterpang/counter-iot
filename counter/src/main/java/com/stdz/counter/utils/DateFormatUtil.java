package com.stdz.counter.utils;

import com.stdz.counter.core.exception.ResultException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateFormatUtil {
    
    /**
     * yyyy-MM-dd
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String STRING_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    
    public static final String CLIENT_TIME_FORMAT3 = "yyyy/MM";

    public static int getDayNumber(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);
        if (startCal.after(endCal)) {
            return 0;
        } else {
            int num = 0;
            startCal.add(Calendar.DAY_OF_YEAR, 1);
            while (startCal.before(endCal)) {
                num = num + 1;
                startCal.add(Calendar.DAY_OF_YEAR, 1);
            }
            return num;
        }
    }
    
    
    /**
     * @param pdttValue 
     * @param pstrDateFormat 
     * @return
     */
    public static String dateToString(Date pdttValue, String pstrDateFormat) {
        String pstrDate = null; // return value
        if (pdttValue == null || "".equals(pdttValue)) {
            return null;
        }
        String formatStyle = null;
        if ((pstrDateFormat == null) || (pstrDateFormat.equals(""))) {
            formatStyle = DATE_FORMAT;
        } else {
            formatStyle = pstrDateFormat;
        }
        SimpleDateFormat oFormatter = new SimpleDateFormat(formatStyle);
        pstrDate = oFormatter.format(pdttValue);
        return pstrDate;
    }
    
    /**
     * 字符串转为日期格式
     * @param dateString 
     * @return
     * @throws ParseException
     */
    public static Date stringFormatDate(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = bartDateFormat.parse(dateString);
        return date;
    }
    
    public static Date stringFormatDate(String dateString, String str) {
        try {
            SimpleDateFormat bartDateFormat = new SimpleDateFormat(str);
            Date date = bartDateFormat.parse(dateString);
            return date;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date stringFormat(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM");
        Date date = bartDateFormat.parse(dateString);
        return date;
    }

    public static Date stringFormatDatesf(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = bartDateFormat.parse(dateString);
        return date;
    }

    /**
     * 字符串转为日期格式
     * @param dateString 
     * @return
     * @throws ParseException
     */
    public static Date stringFormatDateTime(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = bartDateFormat.parse(dateString);
        return date;
    }

    public static String strFormatDateTime(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = bartDateFormat.parse(dateString);
        return bartDateFormat.format(date);
    }

    /**
     * 字符串转为日期格式
     * @param dateString 
     * @return
     * @throws ParseException
     */
    public static Date stringFormatDateHour(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH");
        Date date = bartDateFormat.parse(dateString);
        return date;
    }

    /**
     * 字符串转为日期格式
     * @param dateString 
     * @return
     * @throws ParseException
     */
    public static Date stringFormatDateTime2(String dateString) throws ParseException {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = bartDateFormat.parse(dateString);
        return date;
    }

    /**
     * 将时间格式化为含时分秒的字符串
     * @param date 
     * @return
     * @throws ParseException
     */
    public static String dateTimeFormatString(Date date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String strFormatString(String dateString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(dateString);
        return dateFormat.format(date);
    }

    public static String strFormatString1(String dateString) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = format.parse(dateString);
        return dateFormat.format(date);
    }

    public static String strMonthFormatString(String dateString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date date = dateFormat.parse(dateString);
        return dateFormat.format(date);
    }

    public static String strYMFormatString(String dateString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yy");
        Date date = dateFormat.parse(dateString);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        return format.format(date);
    }

    /**
     * 将时间格式化为含时的字符串
     * @param date 
     * @return
     * @throws ParseException
     */
    public static String dateHourFormatString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH");
        return dateFormat.format(date);
    }
    
    public static String dateFormatString(Date date, String str) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(str);
        return dateFormat.format(date);
    }

    /**
     * 将时间格式化为不含时分秒的字符串
     * @param date 
     * @return
     * @throws ParseException
     */
    public static String dateFormatString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    /**
     * 将时间格式化为不含日时分秒的字符串
     * @param date 
     * @return
     * @throws ParseException
     */
    public static String dateFormatStringm(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        return dateFormat.format(date);
    }

    public static String dateFormatStringsf(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return dateFormat.format(date);
    }

    public static Date getTheDayOfStartTime(String dateString) throws ParseException {
        Date date = null;
        if (dateString.contains(" ")) {
            date = stringFormatDateTime(dateString);
        } else {
            date = stringFormatDate(dateString);
        }
        return getTheDayOfStartTime(date);
    }

    public static Date getTheDayOfStartTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getTheDayOfEndTime(String dateString) throws ParseException {
        Date date = null;
        if (dateString.contains(" ")) {
            date = stringFormatDateTime(dateString);
        } else {
            date = stringFormatDate(dateString);
        }
        return getTheDayOfEndTime(date);
    }
    
    public static Date getDayEndTimeByDate(Date date, int changeDay) {
    	Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        cal.add(Calendar.DAY_OF_YEAR, changeDay);
        return cal.getTime();
    }
    
    public static Date getDayStartTimeByDate(Date date, int changeDay) {
    	Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_YEAR, changeDay);
        return cal.getTime();
    }

    public static Date getTheDayOfEndTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Date dayToDate(Date date, int dValue) {
        if (date == null) {
            date = new Date();
        }
        Calendar bDate = Calendar.getInstance();
        bDate.setTime(date);
        bDate.add(Calendar.DATE, dValue);
        return bDate.getTime();
    }

    /**
     * 获取最近四周(包括本周)格式化后的日期
     * @param gString 
     * @return
     */
    public static String[][] getFourWeekDate(String gString) {
        String[][] weekDate = new String[4][2];
        SimpleDateFormat bartDateFormat = new SimpleDateFormat(gString);
        for (int i = 0; i < 4; i++) {
            Calendar cd = Calendar.getInstance();
            int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK) - 1;
            if (dayOfWeek == 0) {
                dayOfWeek = 7;
            }
            cd.add(Calendar.DATE, -dayOfWeek + 1 - 7 * i);
            weekDate[i][0] = bartDateFormat.format(cd.getTime());
            dayOfWeek = cd.get(Calendar.DAY_OF_WEEK) - 1;
            cd.add(Calendar.DATE, -dayOfWeek + 7);
            weekDate[i][1] = bartDateFormat.format(cd.getTime());
        }
        return weekDate;
    }

    /**获取半小时前日期*/
    public static Date getHalfHourDate() {
        Calendar cd = Calendar.getInstance();
        cd.setTime(new Date());
        cd.add(Calendar.MINUTE, -30);
        return cd.getTime();
    }

    /**获取昨天的日期*/
    public static Date getYesterdayDate() {
        Calendar cd = Calendar.getInstance();
        cd.setTime(new Date());
        cd.add(Calendar.DAY_OF_YEAR, 1);
        return cd.getTime();
    }
    
    public static ArrayList<String> returnDateStrLists(Date beginDate, Date endDate) {
        ArrayList<String> dateList = new ArrayList<String>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        if (beginDate.equals(endDate)) {
            String beginDateStr = format.format(beginDate);
            dateList.add(beginDateStr);
        } else {
            while (beginDate.before(endDate)) {
                String beginDateStr = format.format(beginDate);
                dateList.add(beginDateStr);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(beginDate);
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                beginDate = calendar.getTime();
            }
            //          dateList.add(format.format(beginDate));
        }
        return dateList;
    }

    public static String[] getCurrentMonths() {
        Date endDate = new Date();
        Calendar cd = Calendar.getInstance();
        cd.setTime(endDate);
        cd.add(Calendar.MONTH, -1);
        Date beginDate = cd.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String beginDateStr = dateFormat.format(getTheDayOfStartTime(beginDate));
        String endDateStr = dateFormat.format(getTheDayOfEndTime(endDate));
        return new String[] { beginDateStr, endDateStr };
    }

    public static Date[] getSevenDays() {
        Date date = new Date();
        Calendar bCalendar = Calendar.getInstance();
        Calendar eCalendar = Calendar.getInstance();
        bCalendar.setTime(date);
        eCalendar.setTime(date);
        bCalendar.add(Calendar.DAY_OF_YEAR, -8);
        eCalendar.add(Calendar.DAY_OF_YEAR, -1);
        return new Date[] { bCalendar.getTime(), eCalendar.getTime() };
    }

    public static String dateFormatStringByNY(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        return dateFormat.format(date);
    }

    /**比较两个时间间隔是超过5分钟
     * @param date1 
     * @param date2 
     * @return
     */
    public static Boolean returnCompareDate5(Date date1, Date date2) {
        Calendar cd = Calendar.getInstance();
        cd.setTime(date1);
        long time1 = cd.getTimeInMillis();
        cd.setTime(date2);
        long time2 = cd.getTimeInMillis();
        long compare = time2 - time1;
        if (compare >= 60000 * 5) {
            return true;
        } else {
            return false;
        }
    }

   

    /**
     * 获取年月
     * @param datetime 
     * @return
     */
    public static int getYearMonth(Date datetime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datetime);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int yearmonth = year * 100 + month;
        return yearmonth;
    }

    /**
     * 字符串日期转日期类型,并且十分秒为00:00:00
     * @param beginDateStr 
     * @return
     */
    public static Date strToBeginDate(String beginDateStr) throws ResultException {
        try {
            Date beginDate = stringFormatDate(beginDateStr);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(beginDate);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            beginDate = calendar.getTime();
            return beginDate;
        } catch (Exception e) {
            throw new ResultException("date.error");
        }
    }

    /**
     * 字符串日期转日期类型,并且十分秒为23:59:59
     * @param endDateStr 
     * @return
     */
    public static Date strToEndDate(String endDateStr) throws ResultException {
        try {
            Date endDate = stringFormatDate(endDateStr);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            endDate = calendar.getTime();
            return endDate;
        } catch (Exception e) {
            throw new ResultException("date.error");
        }
    }
    
    public static Date getNextDayTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }
    
    public static Date getTimeAfterDay(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, day);
        return calendar.getTime();
    }
    
    public static Date getChangeMonthEndDay(Date date, int changeMonth) {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.MONTH, changeMonth + 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar.getTime();
    }
    
    public static Date getChangeMonthFirstDay(Date date, int changeMonth) {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.MONTH, changeMonth);
        return calendar.getTime();
    }
    
    public static Date getCurrentWeekStart() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setFirstDayOfWeek(Calendar.MONDAY);
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return calendar.getTime();
    }
    
    public static Date getCurrentWeekEnd() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setFirstDayOfWeek(Calendar.MONDAY);
    	calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return calendar.getTime();
    }
    
    public static Date getChangeWeekStart(Date date, int changeWeek) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.setFirstDayOfWeek(Calendar.MONDAY);
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.add(Calendar.WEEK_OF_YEAR, changeWeek);
        return calendar.getTime();
    }
    
    public static Date getChangeWeekEnd(Date date, int changeWeek) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.setFirstDayOfWeek(Calendar.MONDAY);
    	calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        calendar.add(Calendar.WEEK_OF_YEAR, changeWeek);
        return calendar.getTime();
    }

    public static int getCurrentHour() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static Date getTimeAfterHour(int hour, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.HOUR, hour);

        return calendar.getTime();
    }
}
