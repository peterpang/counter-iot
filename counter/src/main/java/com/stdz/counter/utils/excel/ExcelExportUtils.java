package com.stdz.counter.utils.excel;

import com.stdz.counter.entity.ExcelTemplate;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author peanut
 */
public class ExcelExportUtils {

    public static void exportExcel(String excelName, Map<String, List<ExcelTemplate>> groupLists, String date ,HttpServletRequest request
                                   ) throws IOException, ParseException {
        Set<Map.Entry<String, List<ExcelTemplate>>> entrySet = groupLists.entrySet();
        for (Map.Entry<String, List<ExcelTemplate>> entry : entrySet) {
            List<ExcelTemplate> list = entry.getValue();
            // 声明一个工作簿
            HSSFWorkbook hwb = new HSSFWorkbook();
            // 声明一个单子并命名
            HSSFSheet sheet = hwb.createSheet("计数器上排值统计报表");
            // 给单子名称一个长度
            sheet.setDefaultColumnWidth((short)15);
            // 生成一个样式
            HSSFCellStyle style = hwb.createCellStyle();
            // 创建第一行（也可以成为表头）

            HSSFRow row = sheet.createRow(0);
            // 样式字体居中
            // 给表头第一行一次创建单元格
            HSSFCell cell = row.createCell((short)0);
            cell.setCellValue("名称");
            cell.setCellStyle(style);

            cell = row.createCell((short)1);
            cell.setCellValue("时间");
            cell.setCellStyle(style);

            cell = row.createCell((short)2);
            cell.setCellValue("上排值");
            cell.setCellStyle(style);

            ExcelTemplate template = null;
            int rowNum = 0;
            for (int i = 0; i < list.size(); i++ ) {
                ExcelTemplate curTemplate = list.get(i);
                if (curTemplate != null) {
                    rowNum = rowNum + 1;
                    HSSFRow dataRow = sheet.createRow(rowNum);
                    dataRow.createCell(0).setCellValue(curTemplate.getName());
                    dataRow.createCell(1).setCellValue(curTemplate.getTime());
                    dataRow.createCell(2).setCellValue(curTemplate.getCount());
                }
                template = curTemplate;

            }
            if (template != null) {
                HSSFRow dataRow = sheet.createRow(rowNum);
                dataRow.createCell(0).setCellValue(template.getName());
                dataRow.createCell(1).setCellValue(template.getTime());
                dataRow.createCell(2).setCellValue(template.getCount());
            }
            String dir = "C:\\counter";
            File file = new File(dir);
            while (true) {
                if (!file.isDirectory()) {
                    file.mkdir();
                } else {
                    break;
                }
            }
            date = date.replaceAll(":", "");
            File _file = new File(dir + "\\" + date + "\\");
            while (true) {
                if (!_file.isDirectory()) {
                    _file.mkdir();
                } else {
                    break;
                }
            }

            FileOutputStream output = new FileOutputStream("C:\\counter" + "\\" + date +  "\\" + entry.getKey() + ".xls");
            // 写入磁盘
            hwb.write(output);
            output.flush();
            output.close();
        }
    }
}

