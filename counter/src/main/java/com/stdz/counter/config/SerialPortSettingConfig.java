package com.stdz.counter.config;


import com.stdz.counter.service.DeviceService;
import com.stdz.counter.service.ModbusTypeService;
import com.stdz.counter.service.SerialPortService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author peanut
 */
@Component
public class SerialPortSettingConfig implements ApplicationRunner {

    private static final Logger LOG = LoggerFactory.getLogger(SerialPortSettingConfig.class);

    @Autowired
    SerialPortService serialPortService;

    @Autowired
    ModbusTypeService modbusTypeService;

    @Autowired
    DeviceService deviceService;

    @Override
    public void run(ApplicationArguments args) {
//        try {
//            LOG.debug("开始进入定时器.......");
//            List<String> serialPorts = modbusTypeService.findSerialPorts();
//            LOG.debug("串口查询成功......." + serialPorts.size());
//            if (serialPorts.size() > 0 && serialPorts != null) {
//                for (String serialPort : serialPorts) {
//                    LOG.debug("当前的串口为：{}" , serialPort);
//                    serialPortService.readSerialPortIdData(serialPort);
//                }
//            }
//        } catch (Exception e) {
//            LOG.debug("ApplicationRunner Exception");
//            LOG.debug(e.getMessage());
//            e.printStackTrace();
//        }
    }
}
