package com.stdz.counter.config;

import com.stdz.counter.entity.MeterTask;
import com.stdz.counter.service.DeviceService;
import com.stdz.counter.service.MeterTaskService;
import com.stdz.counter.task.CronTaskRegistrar;
import com.stdz.counter.utils.SchedulingRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author peanut
 */
@Component
public class ScheduleConfig implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(ScheduleConfig.class);

    @Autowired
    MeterTaskService meterTaskService;

    @Autowired
    private CronTaskRegistrar taskRegistrar;

    @Autowired
    DeviceService deviceService;

    @Override
    public void run(String... args) throws Exception {
//        LOG.debug("进入ScheduleConfig.....");
//        List<MeterTask> meterTasks = meterTaskService.findAllCron();
//        if (meterTasks.size() > 0) {
//            for (MeterTask meterTask : meterTasks) {
//                if (!meterTask.getIsSuccess()) {
//                    SchedulingRunnable task = new SchedulingRunnable(meterTask.getCron(), meterTask.getMeterId(), meterTaskService, deviceService);
//                    taskRegistrar.addCronTask(meterTask.getMeterId(), task, meterTask.getCron());
//                }
//            }
//        }
    }
}