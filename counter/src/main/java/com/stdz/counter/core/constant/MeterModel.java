package com.stdz.counter.core.constant;

import com.stdz.counter.core.annotation.SupportModes;

import java.util.ArrayList;
import java.util.List;

public enum MeterModel {

    // float:33
    ST76(1, "ST76",1, 33, 37, 0x20, 0x14, 0xf,0x11),

    // float:37
    CR76(2, "CR76", 1, 37, 0x17, 0x12, 0xe, 0x10);

    private int model;

    private String modelName;

    private int bcdDataPos;

    private int intDataPos;

    private int floatDataPos;

    private int decimalDigitPos;

    private int scalePos;

    private int alarm1Pos;

    private int alarm2Pos;

    MeterModel(int model, String modelName, int bcdDataPos,
               int intDataPos, int floatDataPos, int decimalDigitPos,
               int scalePos, int alarm1Pos, int alarm2Pos) {
        this.model = model;
        this.modelName = modelName;
        this.bcdDataPos = bcdDataPos;
        this.intDataPos = intDataPos;
        this.floatDataPos = floatDataPos;
        this.decimalDigitPos = decimalDigitPos;
        this.scalePos = scalePos;
        this.alarm1Pos = alarm1Pos;
        this.alarm2Pos = alarm2Pos;
    }

    MeterModel(int model, String modelName, int intDataPos, int floatDataPos,
               int decimalDigitPos, int scalePos, int alarm1Pos, int alarm2Pos) {
        this.model = model;
        this.modelName = modelName;
        this.intDataPos = intDataPos;
        this.floatDataPos = floatDataPos;
        this.decimalDigitPos = decimalDigitPos;
        this.scalePos = scalePos;
        this.alarm1Pos = alarm1Pos;
        this.alarm2Pos = alarm2Pos;
    }

    public int getModel() {
        return model;
    }

    public String getModelName() {
        return modelName;
    }

    public int getBcdDataPos() {
        return bcdDataPos;
    }


    public int getIntDataPos() {
        return intDataPos;
    }

    public int getFloatDataPos() {
        return floatDataPos;
    }

    public int getDecimalDigitPos() {
        return decimalDigitPos;
    }

    public int getScalePos() {
        return scalePos;
    }

    public int getAlarm1Pos() {
        return alarm1Pos;
    }

    public int getAlarm2Pos() {
        return alarm2Pos;
    }

    public List<DataMode> getDataModels() {
        try {
            SupportModes supportModes = MeterModel.class.getField(this.toString()).getAnnotation(SupportModes.class);
            if (supportModes == null || supportModes.modes() == null || supportModes.modes().length == 0) {
                return null;
            }
            List<DataMode> dataModes = new ArrayList<>();
            if (bcdDataPos > 0) {
                dataModes.add(DataMode.BCD);
            }
            if (intDataPos > 0) {
                dataModes.add(DataMode.INT32);
            }
            if (floatDataPos > 0) {
                dataModes.add(DataMode.FLOAT);
            }
            return dataModes;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getDataPos(int dataMode) {
        if (dataMode == DataMode.BCD.getMode()) {
            return bcdDataPos;
        } else if (dataMode == DataMode.INT32.getMode()) {
            return intDataPos;
        } else if (dataMode == DataMode.FLOAT.getMode()) {
            return floatDataPos;
        }
        return 0;
    }

    public static List<MeterModel> getMeterModels() {
        List<MeterModel> meterModels = new ArrayList<>(MeterModel.values().length);
        for (MeterModel meterModel : MeterModel.values()) {
            meterModels.add(meterModel);
        }
        return meterModels;
    }

    public static MeterModel getMeterModel(int model) {
        for (MeterModel meterModel : MeterModel.values()) {
            if (model == meterModel.model) {
                return meterModel;
            }
        }
        return null;
    }
}
