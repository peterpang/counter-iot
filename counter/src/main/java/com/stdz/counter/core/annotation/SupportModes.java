package com.stdz.counter.core.annotation;

import com.stdz.counter.core.constant.DataMode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SupportModes {

	DataMode[] modes();
}
