package com.stdz.counter.core.constant;

public enum ClearMode {

    NO_CLEAR(0, "不清零"),

    SINGLE_CLEAR(1, "单清零"),

    BOTH_CLEAR(2, "双清零");

    private int mode;

    private String modeName;

    ClearMode(int mode, String modeName) {
        this.mode = mode;
        this.modeName = modeName;
    }

    public int getMode() {
        return mode;
    }

    public String getModelName() {
        return modeName;
    }

    public static ClearMode getClearMode(int mode) {
        for (ClearMode dataMode : ClearMode.values()) {
            if (dataMode.mode == mode) {
                return dataMode;
            }
        }
        return null;
    }
}
