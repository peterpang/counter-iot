package com.stdz.counter.core.cache;

import com.stdz.counter.entity.MeterInfo;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MeterInfoTempCache implements Serializable {


    private Map<Integer, MeterInfo> comMeterMap = null;

    private static MeterInfoTempCache instance = new MeterInfoTempCache();

    public static MeterInfoTempCache getInstance() {
        return instance;
    }

    private MeterInfoTempCache() {
        comMeterMap = new ConcurrentHashMap<>();
    }

    public void cacheComMeterInfos(int meterId, MeterInfo meterInfo) {
        this.comMeterMap.put(meterId, meterInfo);
    }

    public MeterInfo getMeterInfo(int meterId) {
        if (comMeterMap == null) {
            return null;
        }
        MeterInfo meterInfo = comMeterMap.get(meterId);
        return meterInfo;
    }

    public void clearMeterInfo(int meterId) {
        if (comMeterMap == null) {
            return;
        }
        if (comMeterMap.containsKey(meterId)) {
            comMeterMap.remove(meterId);
        }
    }

}
