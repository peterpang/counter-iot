package com.stdz.counter.core.cache;

import com.stdz.counter.entity.MeterInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MeterInfoCache implements Serializable {

    private List<String> comPorts = null;

    private Map<String, List<MeterInfo>> comMeterMap = null;

    private static MeterInfoCache instance = new MeterInfoCache();

    public static MeterInfoCache getInstance() {
        return instance;
    }

    private MeterInfoCache() {
        comMeterMap = new ConcurrentHashMap<>();
    }

    public void cacheComPorts(List<String> comPorts) {
        this.comPorts = comPorts;
    }

    public void cacheComMeterInfos(String comPort, List<MeterInfo> meterInfos) {
        this.comMeterMap.put(comPort, meterInfos);
    }

    public List<String> getComPorts() {
        if (this.comPorts == null) {
            return null;
        }
        List<String> tempComPorts = new ArrayList<>(this.comPorts.size());
        tempComPorts.addAll(this.comPorts);
        return tempComPorts;
    }

    public List<MeterInfo> getComMeterInfos(String comPort) {
        if (comMeterMap == null) {
            return null;
        }
        List<MeterInfo> comMeterInfos = comMeterMap.get(comPort);
        if (comMeterInfos == null || comMeterInfos.size() == 0) {
            return null;
        }
        List<MeterInfo> tempMeterInfos = new ArrayList<>(comMeterInfos.size());
        tempMeterInfos.addAll(comMeterInfos);
        return tempMeterInfos;
    }

}
