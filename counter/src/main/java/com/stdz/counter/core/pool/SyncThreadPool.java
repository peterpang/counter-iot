package com.stdz.counter.core.pool;

import com.stdz.counter.core.thread.ClearDataThread;
import com.stdz.counter.core.thread.SyncDataThread;
import com.stdz.counter.entity.MeterInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *
 */
public class SyncThreadPool {

	private static transient Logger logger = LoggerFactory.getLogger(SyncThreadPool.class);

	private static final int SYNC_SERVICE_MAX_SIZE = 8;

	private static final int CLEAR_MAX_SIZE = 4;

	private static final ThreadPoolExecutor pool = (ThreadPoolExecutor) Executors.newFixedThreadPool(SYNC_SERVICE_MAX_SIZE);

	private static final ThreadPoolExecutor clearPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(CLEAR_MAX_SIZE);
	
	/**
	 * 提交任务到线程池中
	 * @param
	 */
	public static void submit(String comPort, List<MeterInfo> meterInfos) {
	    pool.submit(new SyncDataThread(comPort, meterInfos));
	}

	public static void clear(String comPort, List<MeterInfo> meterInfos) {
		logger.info("submit comPort:{} clear task ", new Object[]{comPort});
		pool.submit(new ClearDataThread(comPort, meterInfos));
	}
	
	public static int getWaitNum() {
	    return pool.getQueue().size();
	}

	public static int getClearWaitNum() {
		return clearPool.getQueue().size();
	}
}
