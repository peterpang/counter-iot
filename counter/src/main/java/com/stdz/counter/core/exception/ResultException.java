package com.stdz.counter.core.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResultException extends Exception {

    private static transient Logger logger = LoggerFactory.getLogger(ResultException.class);

    private String ret;

    private String msg;

    public ResultException(String msg) {
        this.ret = "E";
        this.msg = msg;
    }

    public ResultException(String ret, String msg) {
        this.ret = ret;
        this.msg = msg;
    }

    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void printLog() {
        logger.error("ResultException[ret:{}, msg:{}]", new Object[]{this.ret, this.msg});
        printStackTrace();
    }
}
