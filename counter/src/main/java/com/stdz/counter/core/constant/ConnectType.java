package com.stdz.counter.core.constant;

import java.util.ArrayList;
import java.util.List;

public enum ConnectType {

    TCP(1, "TCP连接"),

    GPRS(2, "GPRS连接"),

    COM(3, "串口连接");

    private int type;

    private String connectName;

    ConnectType(int type, String connectName) {
        this.type = type;
        this.connectName = connectName;
    }

    public int getType() {
        return type;
    }

    public String getConnectName() {
        return connectName;
    }

    public static List<ConnectType> getConnectTypes() {
        List<ConnectType> connectTypes = new ArrayList<>(ConnectType.values().length);
        for (ConnectType connectType : ConnectType.values()) {
            connectTypes.add(connectType);
        }
        return connectTypes;
    }
}
