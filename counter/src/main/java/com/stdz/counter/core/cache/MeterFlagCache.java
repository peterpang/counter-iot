package com.stdz.counter.core.cache;

import java.util.HashMap;
import java.util.Map;

public class MeterFlagCache {

    private static Map<Integer, Integer> onlineFlagMap = new HashMap<Integer, Integer>();

    public static void setMeterFlag(int meterId, int onlineFlag) {
        onlineFlagMap.put(meterId, onlineFlag);
    }

    public static boolean isMeterOnline(int meterId) {
        if (!onlineFlagMap.containsKey(meterId)) {
            return false;
        }
        int flag = onlineFlagMap.get(meterId);
        if (flag == 1) {
            return true;
        }
        return false;
    }
}
