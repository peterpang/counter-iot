package com.stdz.counter.core.constant;

public enum ClearLoopType {

    SINGLE_OPERATE(0, "单次清零", 120000),

    LOOP_OPERTE(1, "循环清零", 120000);

    private int loopType;

    private String loopName;

    // 允许清理时间误差（超时）
    private long clearOverTime;

    ClearLoopType(int loopType, String loopName, long clearOverTime) {
        this.loopType = loopType;
        this.loopName = loopName;
        this.clearOverTime = clearOverTime;
    }

    public int getLoopType() {
        return loopType;
    }

    public String getLoopName() {
        return loopName;
    }

    public long getClearOverTime() {
        return clearOverTime;
    }

    public static ClearLoopType getClearLoopType(int loopType) {
        for (ClearLoopType clearLoopType : ClearLoopType.values()) {
            if (clearLoopType.loopType == loopType) {
                return clearLoopType;
            }
        }
        return null;
    }
}
