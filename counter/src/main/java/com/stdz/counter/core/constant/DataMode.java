package com.stdz.counter.core.constant;

import javax.xml.crypto.Data;

public enum DataMode {


    BCD(1, "BCD模式"),

    INT32(2, "32位整数"),

    FLOAT(3, "32位浮点数");

    private int mode;

    private String modeName;

    DataMode(int mode, String modeName) {
        this.mode = mode;
        this.modeName = modeName;
    }

    public int getMode() {
        return mode;
    }

    public String getModeName() {
        return modeName;
    }

    public static DataMode getDataMode(int mode) {
        for (DataMode dataMode : DataMode.values()) {
            if (dataMode.mode == mode) {
                return dataMode;
            }
        }
        return null;
    }
}
