package com.stdz.counter.entity;


import lombok.Getter;
import lombok.Setter;


/**
 * @author peanut
 */
@Getter
@Setter
public class LabelOption {

    private final Boolean show = false;

    private final String position = "insideBottom";

    private final int distance = 15;

    private final String align = "left";

    private final String verticalAlign = "middle";

    private final int rotate = 90;

    private final String formatter = "{c}  {name|{a}}";

    private final int fontSize = 16;

    private Rich rich ;

    class Rich {
        // Map<String, TextBorderColor> map = new HashMap<>();
        TextBorderColor textBorderColor;
        Rich(){
            textBorderColor = new TextBorderColor();
        }
        class TextBorderColor{
            String textBorderColor;
            TextBorderColor() {
                this.textBorderColor = "#fff";
            }
        }
    }
}
