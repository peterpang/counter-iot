package com.stdz.counter.entity;

import cn.hutool.json.JSONArray;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author peanut
 */
@Getter
@Setter
public class ColumnarData {

    private String name;

    private String type;

    private LabelOption label;

    private JSONArray data;

    private List<String> dateData;

}
