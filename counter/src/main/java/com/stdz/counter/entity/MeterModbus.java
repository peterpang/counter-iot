package com.stdz.counter.entity;

import java.io.Serializable;

/**
 *
 */
public class MeterModbus extends BaseEntity implements Serializable {

    private Long id;

    private String ip;

    private String port;

    private Integer type;

    private String baudRate;

    private String mtId;

    private String name;;

    private String whetherBCD;

    // Integer
    private String registerAddress;

    private String time;

    private String upperRowInt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(String baudRate) {
        this.baudRate = baudRate;
    }

    // Integer


    public String getMtId() {
        return mtId;
    }

    public void setMtId(String mtId) {
        this.mtId = mtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWhetherBCD() {
        return whetherBCD;
    }

    public void setWhetherBCD(String whetherBCD) {
        this.whetherBCD = whetherBCD;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUpperRowInt() {
        return upperRowInt;
    }

    public void setUpperRowInt(String upperRowInt) {
        this.upperRowInt = upperRowInt;
    }
}
