package com.stdz.counter.entity;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author peanut
 */
@Getter
@Setter
public class MeterRecord {

    private Long id;

    private Integer meterId;

    private Date createTime;

    private String upperRow;

    private String name;

    private Date MINUTE;

    private Date SECOND;

    private Date HOUR;

    private Date DAY;

    private Date MONTH;

}
