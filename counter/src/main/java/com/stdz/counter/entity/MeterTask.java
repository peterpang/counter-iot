package com.stdz.counter.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author peanut
 */

@Getter
@Setter
public class MeterTask {

    private Long id;

    private Integer meterId;

    private String cron;

    private Boolean isSuccess;

    private String type;
}
