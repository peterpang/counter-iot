package com.stdz.counter.entity;

import java.io.Serializable;

/**
 *
 */
public class Param implements Serializable {
    // 配置id
    private String paramKey;
    // 名称
    private String paramValue;

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
