package com.stdz.counter.entity;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author peanut
 */
@Getter
@Setter
public class ModbusType extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 8656128222714547171L;

    private Long id;

    private String ip;

    private String port;

    private Integer type;

    private String baudRate;
}
