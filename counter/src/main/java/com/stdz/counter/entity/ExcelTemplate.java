package com.stdz.counter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author peanut
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ExcelTemplate {

    private static final long serialVersionUID = 1L;

    private String name;

    private String time;

    private String count;

}
