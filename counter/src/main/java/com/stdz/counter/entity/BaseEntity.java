package com.stdz.counter.entity;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author peanut
 */
@Getter
@Setter
public class BaseEntity {

    private Date createTime;

    private Date updateTime;
}
