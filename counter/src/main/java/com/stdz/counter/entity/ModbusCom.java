package com.stdz.counter.entity;

import com.stdz.counter.core.constant.MeterModel;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class ModbusCom implements Serializable {
    // 配置id
    private int id;
    // 名称
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
