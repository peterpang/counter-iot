package com.stdz.counter.entity;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author peanut
 */
@Getter
@Setter
public class Meter extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 8656128222714547171L;

    private Long id;

    private String name;

    private Long modbusId;

    private String upperRow;

    private String lowerRow;

    private String al1;

    private String al2;

    private String suspend;

    private String clearMode;

    private String whetherBCD;

    private String registerAddress;

    private String time;

    private String upperRowInt;

    private String offLine;

    private String scal;
}
