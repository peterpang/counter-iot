package com.stdz.counter.dao;


import com.stdz.counter.entity.CurveData;
import com.stdz.counter.entity.ExcelTemplate;
import com.stdz.counter.entity.MeterRecord;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author peanut
 */
public interface MeterHDao {
    List<MeterRecord> findByDate(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("meterId") Integer meterId);

    List<ExcelTemplate> findByDay(@Param("type") String type, @Param("date") String date,@Param("laterDate")String laterDate, @Param("meterIds") String meterIds);

    public MeterRecord queryMeterH(@Param("meterId") int meterId, @Param("recordTime") Date recordTime);

    public int updateMeterH(@Param("id") long id, @Param("upperRow") String upperRow);

    public void addMeterH(MeterRecord meterRecord);

    public List<MeterRecord> queryHourDataByDate(@Param("meterId") int meterId, @Param("queryTime") String queryTime);

    List<ExcelTemplate> exportHourDataByDate(@Param("queryTime") String queryTime, @Param("meterIds") String meterIds);
}
