package com.stdz.counter.dao;

import com.github.pagehelper.Page;
import com.stdz.counter.entity.MeterInfo;
import com.stdz.counter.entity.MeterModbus;
import com.stdz.counter.entity.ModbusType;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface MeterInfoDao {

    /**
     * 查询所有的端口
     * @return
     */
    public abstract List<String> queryAllComPorts();

    /**
     * 查询所有的设备
     * @return
     */
    public abstract List<MeterInfo> queryAllMeterInfos();

    public abstract MeterInfo queryMeterInfo(@Param("meterId") int meterId);

    public abstract Integer findByOnline();

    public abstract Page<MeterInfo> queryAll();

    /**
     * 根据端口查询所有的设备
     * @param comPort
     * @return
     */
    public abstract List<MeterInfo> queryMeterInfosByCom(@Param("comPort") String comPort);

    /**
     * 新增设备
     * @param meterInfo
     * @return
     */
    public abstract int injectMeterInfo(MeterInfo meterInfo);

    /**
     * 更新设备信息
     * @param meterInfo
     * @return
     */
    public abstract int updateMeterInfo(MeterInfo meterInfo);

    public abstract int syncMeterData(MeterInfo meterInfo);

    /**
     * 暂停/继续
     * @param meterId
     * @param suspendFlag
     * @return
     */
    public abstract int updateSuspendFlag(@Param("meterId") int meterId, @Param("suspendFlag") int suspendFlag);

    /**
     * 删除设备
     * @param meterId
     * @return
     */
    public abstract int delMeterInfo(@Param("meterId") int meterId);

    public abstract int updateClearOver(MeterInfo meterInfo);

    public abstract int updateClearSuccess(MeterInfo meterInfo);
}
