package com.stdz.counter.dao;

import com.stdz.counter.entity.MeterTask;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author peanut
 */
public interface MeterTaskDao {

    void addMeterTask(MeterTask meterTask);

    List<MeterTask> findAllCron();

    MeterTask findByMeterId(@Param("meterId") Integer meterId);

    void updateByMeterId(@Param("meterId") Integer meterId);

    void delete(@Param("meterId")Integer id);
}
