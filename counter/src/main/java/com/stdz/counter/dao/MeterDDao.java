package com.stdz.counter.dao;


import com.stdz.counter.entity.MeterRecord;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface MeterDDao {
    List<MeterRecord> findByDate(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("meterId") Integer meterId);

    public MeterRecord queryMeterD(@Param("meterId") int meterId, @Param("recordTime") Date recordTime);

    public int updateMeterD(@Param("id") long id, @Param("upperRow") String upperRow);

    public void addMeterD(MeterRecord meterRecord);

    public List<MeterRecord> queryDayDataByMonth(@Param("meterId") int meterId, @Param("queryTime") String queryTime);
}
