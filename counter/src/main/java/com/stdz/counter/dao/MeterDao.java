package com.stdz.counter.dao;

import com.github.pagehelper.Page;
import com.stdz.counter.entity.Meter;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @author peanut
 */
public interface MeterDao {

    void addMeter(Meter meter);

    /**
     * 通过id去修改仪表上排和下排
     * @param id
     * @param upperRow
     * @param lowerRow
     */
    void updateValues(@Param("id") Long id, @Param("upperRow") String upperRow,
                      @Param("lowerRow") String lowerRow, @Param("Al1") String Al1,
                      @Param("Al2") String Al2,@Param("scal") String scal,
                      @Param("offLine") String offLine);

    /**
     * 获取所有的计数器
     * @return
     */
    Page<Meter> findAll();

    /**
     * 获取单个的计数器
     * @param id
     * @return
     */
    Map findById(@Param("id") Long id);

    /**
     * 修改暂停的标识
     * @param id
     */
    void updateSuspend(@Param("id") Long id);

    /**
     * 通过id去删除仪表
     * @param id
     */
    void delete(@Param("id") Long id);

    void setCounter(Map<String, Object> params);

    Integer findByCom(Map map);

    List<Meter> findAlls();

    Long findByOnLine();
}
