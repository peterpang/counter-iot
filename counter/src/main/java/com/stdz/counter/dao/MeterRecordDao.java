package com.stdz.counter.dao;


import com.stdz.counter.entity.MeterRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface MeterRecordDao {

    MeterRecord findById(Integer mId);

    void addMeterRecord(MeterRecord meterRecord);

    List<MeterRecord> findByDate(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("meterId") Integer meterId);

    public List<MeterRecord> querySecondDataByMinute(@Param("meterId") int meterId, @Param("queryTime") String queryTime);
}
