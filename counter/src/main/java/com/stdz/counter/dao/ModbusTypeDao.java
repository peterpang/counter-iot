package com.stdz.counter.dao;

import com.stdz.counter.entity.MeterModbus;
import com.stdz.counter.entity.ModbusType;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author peanut
 */
public interface ModbusTypeDao {

    /**
     *  添加设备通讯接口
     * @param modbusType
     */
    void addModbus(ModbusType modbusType);

    /**
     * 获取所有的通讯接口以及相对应的仪表
     * @return
     */
    List<MeterModbus> findAllLinkModbusType(@Param("port") String com);

    /**
     * 获取到所有的串口
     * @return
     */
    List<String> findSerialPorts();

    /**
     * 通过id去删除通讯仪表
     * @param modbusId
     */
    void delete(@Param("id")Integer modbusId);

    List<String> findAllComs();

    List<Map<String, Object>> findAllModbusType();
}
