package com.stdz.counter.dao;


import com.stdz.counter.entity.MeterRecord;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author peanut
 */
public interface MeterYDao {
    List<MeterRecord> findByDate(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("meterId") Integer meterId);

    public MeterRecord queryMeterY(@Param("meterId") int meterId, @Param("recordTime") Date recordTime);

    public int updateMeterY(@Param("id") long id, @Param("upperRow") String upperRow);

    public void addMeterY(MeterRecord meterRecord);

    public List<MeterRecord> queryMonthDataByYear(@Param("meterId") int meterId, @Param("queryTime") String queryTime);
}
