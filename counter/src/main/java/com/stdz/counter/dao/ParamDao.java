package com.stdz.counter.dao;

import com.github.pagehelper.Page;
import com.stdz.counter.entity.Meter;
import com.stdz.counter.entity.Param;

import java.util.List;
import java.util.Map;


/**
 * @author peanut
 */
public interface ParamDao {

    public List<Param> queryAllParams();
}
