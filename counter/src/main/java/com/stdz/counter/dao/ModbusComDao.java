package com.stdz.counter.dao;

import com.github.pagehelper.Page;
import com.stdz.counter.entity.MeterInfo;
import com.stdz.counter.entity.ModbusCom;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author peanut
 */
public interface ModbusComDao {

    /**
     * 查询所有的端口
     * @return
     */
    public abstract List<ModbusCom> getAllComPorts();
}
