package com.stdz.counter.dao;


import com.stdz.counter.entity.CurveData;
import com.stdz.counter.entity.MeterRecord;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author peanut
 */
public interface MeterMDao {
    List<MeterRecord> findByDate(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("meterId") Integer meterId);

    /**
     *
     * @param startTime
     * @param endTime
     * @param meterIds
     * @return
     */
    List<MeterRecord> findByDateIds(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("meterIds") String meterIds);

    public MeterRecord queryMeterM(@Param("meterId") int meterId, @Param("recordTime") Date recordTime);

    public int updateMeterM(@Param("id") long id, @Param("upperRow") String upperRow);

    public void addMeterM(MeterRecord meterRecord);

    public List<MeterRecord> queryMinuteDataByHour(@Param("meterId") int meterId, @Param("queryTime") String queryTime);

}
